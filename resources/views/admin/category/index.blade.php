@extends('admin.layouts.app')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card my-4">
                    <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
                        <div class="bg-gradient-primary shadow-primary border-radius-lg pt-4 pb-3">
                            <h6 class="text-white text-capitalize ps-3">Category</h6>
                        </div>
                    </div>
                    <div class="pt-2 d-flex justify-content-between">
                        @hasPermission('create_category')
                        <div id="model-create" class="modal fade" tabindex="-1" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title text-dark" id="exampleModalLabel">Create Category</h5>
                                        <button type="button" class="close btn btn-close-modal" aria-label="Close">
                                            <span aria-hidden="true">X</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <form id="form-create-category" data-action="{{ route('categories.store') }}">
                                            <div class="col-xs-12 col-sm-12 col-md-12 mt-3">
                                                <div class="form-group custom-form-group-create">
                                                    <strong class="form-check" style="padding-left: 0px">Name: </strong>
                                                    <div
                                                        class="form-check input-group input-group-dynamic info-horizontal" style="padding-left: 0px">
                                                        <input type="text" name="name" id="name_create"
                                                               class="form-control shadow-none" style="outline: none"
                                                               onkeyup="myFunction()"
                                                               onfocus="focused(this)" onfocusout="defocused(this)">
                                                    </div>
                                                    <div class="text-danger name_error c_errors"></div>
                                                </div>
                                            </div>
                                            <input type="hidden" name="slug" id="slug_create" value=""
                                                   class="form-control shadow-none" style="outline: none"
                                                   onfocus="focused(this)" onfocusout="defocused(this)">
                                            <div class="form-group">
                                                <label for="" class="text-lg text-bold m-0 my-2 text-dark">Parent
                                                    Category</label>
                                                <select class="form-select form-select-sm shadow-none p-0"
                                                        name="parent_id" id="parent_id"
                                                        style="border-top: none;border-right: none;border-left: none;">
                                                    <option selected value="">None</option>
                                                    @foreach($parentCategory as $item)
                                                        <option value="{{$item->id}}">{{$item->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-dark btn-close-modal" data-dismiss="modal">
                                            Close
                                        </button>
                                        <button type="button" class="btn btn-danger btn-store-category">Add</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endhasPermission
                        <!-- Update modal -->
                        @hasPermission('update_category')
                        <div id="model-update" class="modal fade" tabindex="-1" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title text-dark" id="exampleModalLabel">Update Category</h5>
                                        <button type="button" class="close btn btn-close-modal" aria-label="Close">
                                            <span aria-hidden="true">X</span>
                                        </button>
                                    </div>
                                    <div class="modal-body" id="edit-form-category">

                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-dark btn-close-modal" data-dismiss="modal">
                                            Close
                                        </button>
                                        <button type="button" class="btn btn-danger btn-update-category">Update</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endhasPermission
                        <div>
                            @hasPermission('create_category')
                            <button type="button" id="btn-show-modal-create" class="btn btn-danger btnCreate"
                                    style="margin-left: 18px;margin-top: 10px;">
                                Create
                            </button>
                            @endhasPermission
                        </div>
                        <div>
                            <div class="input-group">
                                <form class="d-flex align-items-center form_search">
                                    <div class="my-3">
                                        <input type="text" placeholder="Search" id="name-search" name="name"
                                               value="{{ request('name') }}" class="form-control shadow-none">
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div></div>
                    </div>
                    <div id="list" data-action="{{ route('categories.list') }}">

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script src="/js/admin/category.js"></script>
@endsection
