<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    protected $fillable = [
        'name',
        'email',
        'address',
        'dob',
        'phone',
        'gender',
        'password',
    ];

    protected $hidden = [
        'password',
        'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }

    public function scopeWithName($query, $name)
    {
        return $name ? $query->where('name', 'like', "%" . $name . "%") : null;
    }

    public function scopeWithRoleId($query, $roleId)
    {
        return $roleId ? $query->WhereHas('roles', function ($q) use ($roleId) {
            $q->where('role_id', $roleId);
        }) : null;
    }

    public function hasPermission($permissionName)
    {
        foreach ($this->roles as $role) {
            if ($role->hasPermission($permissionName)) {
                return true;
            }
        }
        return false;
    }

    public function isSuperAdmin()
    {
        return $this->hasRole('super_admin');
    }

    public function hasRole($role)
    {
        return $this->roles->contains('name', $role);
    }

    public function attachRole($roleId)
    {
        return $this->roles()->attach($roleId);
    }

    public function syncRole($roleId)
    {
        return $this->roles()->sync($roleId);
    }

    public function detachRole()
    {
        return $this->roles()->detach();
    }

    public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = Hash::make($password);
    }
}
