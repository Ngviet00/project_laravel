<?php

namespace Tests\Feature\Roles;

use App\Models\Role;
use Illuminate\Http\Response;
use Tests\TestCase;

class UpdateRoleTest extends TestCase
{
    /** @test */
    public function unauthenticated_can_not_view_form_edit_role()
    {
        $roleCreated = $this->createFactoryRole();
        $response = $this->get($this->getEditRoleRoute($roleCreated['id']));
        $response->assertRedirect(route('login'));
    }

    /** @test */
    public function authenticated_without_permission_can_not_view_form_edit_role()
    {
        $this->loginUser();
        $roleCreated = $this->createFactoryRole();
        $response = $this->get($this->getEditRoleRoute($roleCreated['id']));
        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    /** @test */
    public function authenticated_with_permission_can_view_form_edit_role()
    {
        $this->loginUserWithPermission('edit_role');
        $roleCreated = $this->createFactoryRole();
        $response = $this->get($this->getEditRoleRoute($roleCreated['id']));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('admin.role.edit');
    }

    /** @test */
    public function authenticated_without_permission_can_not_update_role()
    {
        $this->loginUser();
        $existsRole = $this->createFactoryRole();
        $dataUpdate = $this->makeFactoryRole();;
        $response = $this->put(route('roles.update', $existsRole['id']), $dataUpdate);
        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    /** @test */
    public function authenticated_with_permission_can_not_update_role_if_name_is_null()
    {
        $this->loginUserWithPermission('update_role');
        $existsRole = $this->createFactoryRole();
        $dataUpdate = $this->makeFactoryRoleSetFieldNull('name');
        $response = $this->put(route('roles.update', $existsRole['id']), $dataUpdate);
        $response->assertSessionHasErrors(['name']);
    }

    /** @test */
    public function authenticated_with_permission_can_update_role_if_data_is_valid()
    {
        $this->loginUserWithPermission('update_role');
        $existsRole = $this->createFactoryRole();
        $dataUpdate = $this->makeFactoryRole();
        $response = $this->put(route('roles.update', $existsRole['id']), $dataUpdate);
        $this->assertDatabaseHas('roles', $dataUpdate);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect(route('roles.index'));
    }

    public function getEditRoleRoute($id): string
    {
        return route('roles.edit', $id);
    }

    public function updateRoleRoute(): string
    {
        return route('roles.update');
    }

    public function createFactoryRole(): array
    {
        return Role::factory()->create()->toArray();
    }
    public function makeFactoryRole(): array
    {
        return Role::factory()->make()->toArray();
    }
    public function makeFactoryRoleSetFieldNull($field): array
    {
        return Role::factory()->make([
            $field => null
        ])->toArray();
    }
}
