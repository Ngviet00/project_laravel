<?php

namespace App\Http\Composers;

use App\Repositories\RoleRepository;
use Illuminate\View\View;

class RoleComposer
{

    protected $roleRepository;

    public function __construct(RoleRepository $roleRepository)
    {
        $this->roleRepository = $roleRepository;
    }

    public function compose(View $view)
    {
        $view->with('countRole', $this->roleRepository->count());
        $view->with('roles', $this->roleRepository->all());
    }
}
