@extends('admin.layouts.app')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card my-4">
                    <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
                        <div class="bg-gradient-primary shadow-primary border-radius-lg pt-4 pb-3">
                            <h6 class="text-white text-capitalize ps-3">Quản lý đơn hàng</h6>
                        </div>
                    </div>

                    <div class="input-group justify-content-center">
                        <form class="d-flex align-items-center form_search">
                            <div class="my-3 d-flex">
                                <input type="text"
                                       placeholder="Search"
                                       id="name-search"
                                       name="id_order"
                                       value="{{ request('name') }}"
                                       class="form-control shadow-none"
                                >
                                <select name="status">
                                    <option value="">--Chọn--</option>
                                    <option value="1">Chờ xác nhận</option>
                                    <option value="2">Đang giao hàng</option>
                                    <option value="3">Đã giao</option>
                                    <option value="4">Đã hủy</option>
                                </select>
                                <button style="width: 150px;
                                    background: blue;
                                    color: white;
                                    border: none;
                                    border-radius: 3px;"
                                >
                                    Tim kiem
                                </button>
                                <a href="{{route('listOrder')}}" style="width: 250px; margin-left: 10px;margin-top: 10px;">Xóa tìm kiếm</a>
                            </div>
                        </form>
                    </div>

                    <div class="pt-3 d-flex justify-content-between">
                        <div>
                            @if(Session::has('message'))
                                <p class="fw-bold text-success pt-2"
                                   style="padding-left:17px">{!! Session::get('message') !!}
                                </p>
                            @endif
                        </div>
                    </div>

                    <div class="px-0 pb-2">
                        <div class="table-responsive p-0">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th scope="col">Mã đơn hàng</th>
                                    <th scope="col">Sản phẩm</th>
                                    <th scope="col">Giá</th>
                                    <th scope="col">Ngày đặt mua</th>
                                    <th scope="col">Trạng thái</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($listOrderHistory as $item)
                                    <tr style="vertical-align: middle">
                                        <th scope="row" style="text-align: center">{{$item->id}}</th>
                                        <td class="d-flex align-items-center">
                                            <img src="{{$item->image}}" alt=""
                                                 style="width: 80px ; padding-right: 10px;">
                                            {{$item->name }} x <strong>{{ $item->quantity}}</strong>
                                        </td>
                                        <td>{{number_format($item->price)}}</td>
                                        <td>{{$item->date_order}}</td>
                                        <td>
                                            @if($item->status == 1)
                                                <span style="color: black;font-weight: bold">Chờ xác nhận</span>
                                                <div>
                                                    <a href="{{route('detailOrder',$item->id)}}"
                                                       class="d-inline-block text-dark"><i class="fas fa-eye"></i></a>
                                                    <a href="{{route('updateOrder',['id' => $item->id,'value' =>2])}}"
                                                       class="d-inline-block mx-2 text-dark"><i class="fas fa-car"></i></a>
                                                    <a href="{{route('updateOrder',['id' => $item->id,'value' =>3])}}"
                                                       class="d-inline-block text-dark"><i class="fas fa-check"></i></a>
                                                    <a href="{{route('updateOrder',['id' => $item->id,'value' =>4])}}"
                                                       class="d-inline-block mx-2 text-dark"><i
                                                            class="fas fa-times"></i></a>
                                                </div>
                                            @elseif($item->status == 2)
                                                <span class="text-primary fw-bold">Đang giao hàng</span>
                                                <div>
                                                    <a href="{{route('detailOrder',$item->id)}}"
                                                       class="d-inline-block text-dark"><i class="fas fa-eye"></i></a>
                                                    <a href="{{route('updateOrder',['id' => $item->id,'value' =>2])}}"
                                                       class="d-inline-block mx-2 text-dark @if($item->status == 2) d-none @endif"><i
                                                            class="fas fa-car"></i></a>
                                                    <a href="{{route('updateOrder',['id' => $item->id,'value' =>3])}}"
                                                       class="d-inline-block text-dark"><i class="fas fa-check"></i></a>
                                                    <a href="{{route('updateOrder',['id' => $item->id,'value' =>4])}}"
                                                       class="d-inline-block mx-2 text-dark"><i
                                                            class="fas fa-times"></i></a>
                                                </div>
                                            @elseif($item->status == 3)
                                                <span class="text-success fw-bold">Đã giao</span>
                                                <div>
                                                    <a href="{{route('detailOrder',$item->id)}}"
                                                       class="d-inline-block text-dark"><i class="fas fa-eye"></i></a>
                                                    @if($item->status != 3)
                                                        <a href="{{route('updateOrder',['id' => $item->id,'value' =>2])}}"
                                                           class="d-inline-block mx-2 text-dark"><i
                                                                class="fas fa-car"></i></a>
                                                        <a href="{{route('updateOrder',['id' => $item->id,'value' =>3])}}"
                                                           class="d-inline-block"><i class="text-dark fas fa-check"></i></a>
                                                        <a href="{{route('updateOrder',['id' => $item->id,'value' =>4])}}"
                                                           class="d-inline-block mx-2 text-dark"><i
                                                                class="fas fa-times"></i></a>
                                                    @endif
                                                </div>
                                            @else
                                                <span class="text-danger fw-bold">Đã hủy</span>
                                                <div>
                                                    <a href="{{route('detailOrder',$item->id)}}"
                                                       class="d-inline-block text-dark"><i class="fas fa-eye"></i></a>
                                                    @if($item->status != 4)
                                                        <a href="{{route('updateOrder',['id' => $item->id,'value' =>2])}}"
                                                           class="d-inline-block mx-2 text-dark"><i
                                                                class="fas fa-car"></i></a>
                                                        <a href="{{route('updateOrder',['id' => $item->id,'value' =>3])}}"
                                                           class="d-inline-block text-dark"><i class="fas fa-check"></i></a>
                                                        <a href="{{route('updateOrder',['id' => $item->id,'value' =>4])}}"
                                                           class="d-inline-block mx-2 text-dark"><i
                                                                class="fas fa-times"></i></a>
                                                    @endif
                                                </div>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <style>
        /*tbody tr{*/
        /*    text-align: center;*/
        /*}*/
        .switch {
            position: relative;
            display: inline-block;
            width: 60px;
            height: 34px;
        }

        .switch input {
            opacity: 0;
            width: 0;
            height: 0;
        }

        .slider {
            position: absolute;
            cursor: pointer;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: #ccc;
            -webkit-transition: .4s;
            transition: .4s;
        }

        .slider:before {
            position: absolute;
            content: "";
            height: 26px;
            width: 26px;
            left: 4px;
            bottom: 4px;
            background-color: white;
            -webkit-transition: .4s;
            transition: .4s;
        }

        input:checked + .slider {
            background-color: #2196F3;
        }

        input:focus + .slider {
            box-shadow: 0 0 1px #2196F3;
        }

        input:checked + .slider:before {
            -webkit-transform: translateX(26px);
            -ms-transform: translateX(26px);
            transform: translateX(26px);
        }

        /* Rounded sliders */
        .slider.round {
            border-radius: 34px;
        }

        .slider.round:before {
            border-radius: 50%;
        }
    </style>
@endsection
@section('js')
    <script src="/js/admin/slide.js"></script>
@endsection
