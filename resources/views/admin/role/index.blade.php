@extends('admin.layouts.app')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card my-4">
                    <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
                        <div class="bg-gradient-primary shadow-primary border-radius-lg pt-4 pb-3">
                            <h6 class="text-white text-capitalize ps-3">Role</h6>
                        </div>
                    </div>

                    <div class="pt-3 d-flex justify-content-between">
                        <div>
                            @if(Session::has('message'))
                                <p class="fw-bold text-success pt-2"
                                   style="padding-left:17px">{!! Session::get('message') !!}
                                </p>
                            @endif
                        </div>
                        <div>
                            <div class="input-group">
                                <form action="" method="GET" class="d-flex align-items-center">
                                    <div class="form-outline d-flex">
                                        <input type="search" id="name" name="name" value="{{ request('name') }}"
                                               class="form-control pl-2 shadow-none"
                                               placeholder="Search"/>
                                    </div>
                                    <button type="submit" class="btn btn-primary m-0">
                                        <i class="fas fa-search"></i>
                                    </button>
                                </form>
                            </div>
                        </div>
                        <a href="{{route('roles.create')}}" class="btn btn-danger d-inline-block text-capitalize"
                           style="margin-right:17px">Create</a>
                    </div>

                    <div class="px-0 pb-2">
                        <div class="table-responsive p-0">
                            <table class="table align-items-center mb-0" style="table-layout: fixed; width: 100%">
                                <thead>
                                <tr>
                                    <th width="70"
                                        class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                        ID
                                    </th>
                                    <th width="220"
                                        class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
                                        Name
                                    </th>
                                    <th width="650"
                                        class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                        Permissions
                                    </th>
                                    <th
                                        class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                        Action
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($roles as $role)
                                    <tr>
                                        <td style="display:table-cell;vertical-align:top;height: 30px">
                                            <div style="padding-left: 20px; height: 100%"
                                                 class="text-sm font-weight-bold mb-0">
                                                {{ $role->id }}
                                            </div>
                                        </td>
                                        <td style="display:table-cell;vertical-align:top;height: 30px">
                                            <div class="text-sm font-weight-bold mb-0"
                                                 style="height: 100%">{{ $role->name }}</div>
                                        </td>
                                        <td class="align-middle text-center text-sm"
                                            style="display:table-cell;vertical-align:top;height: 30px">
                                            <div class="d-flex flex-wrap" style="height: 100%">
                                                @if($role->id == 1)
                                                    <span></span>
                                                @else
                                                    @foreach($role->permissions as $key => $permission)
                                                        <span style="font-size: 14px;">
                                                        {{ $permission->name }},&nbsp&nbsp
                                                    </span>
                                                    @endforeach
                                                @endif

                                            </div>
                                        </td>
                                        <td class="align-middle d-flex justify-content-around align-items-center pt-3">
                                            @hasPermission('show_role')
                                            <a href="{{ route('roles.show', $role->id) }}"
                                               class="text-secondary mr-2 d-inline-block font-weight-bold text-sm text-decoration-none">
                                                <i class="fas fa-eye"></i>
                                            </a>
                                            @endhasPermission

                                            @hasPermission('edit_role')
                                            <a href="{{ route('roles.edit', $role->id) }}"
                                               class="text-secondary mr-2 font-weight-bold text-sm d-inline-block text-decoration-none">
                                                <i class="fas fa-edit"></i>
                                            </a>
                                            @endhasPermission

                                            @hasPermission('delete_role')
                                            <form action="{{route('roles.delete', $role->id)}}" method="POST">
                                                @csrf
                                                @method('DELETE')
                                                <button class='btn m-0 text-sm'
                                                        onclick="return confirm('Are you sure?')">
                                                    <i class="fas fa-trash-alt text-secondary"></i>
                                                </button>
                                            </form>
                                            @endhasPermission
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            @if(count($roles) == 0)
                                <div class="py-3" style="margin-left: 40%;">
                                    <p style="margin-left: 22px !important;" class="m-0 fw-bold text-danger">No Data</p>
                                </div>
                            @endif
                            <div class="d-flex justify-content-center">
                                {{ $roles->appends(request()->all())->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script src="/js/admin/role.js"></script>
@endsection
