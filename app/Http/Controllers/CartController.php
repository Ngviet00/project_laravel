<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;

class CartController extends Controller
{

    public function index()
    {
        return view('client.cart');
    }

    public function addCart(Request $request)
    {
        $product = Product::where('id', $request->id)->first();
        $cart = session()->get('cart', []);

        if (isset($cart[$request->id . '_' . $request->size])) {
            $cart[$request->id . '_' . $request->size]['quantity']++;
        } else {
            $cart[$request->id . '_' . $request->size] = [
                'id' => $request->id,
                'name' => $request->name,
                'brand' => $request->brand,
                'quantity' => $request->quantity,
                'price' => $product->price,
                'image' => $request->image,
                'size' => $request->size,
                'total' => (int)$request->quantity * (int)$product->price
            ];

        }
        session()->put('cart', $cart);
        return response()->json($cart);
    }

    public function deleteCart($id): \Illuminate\Http\JsonResponse
    {
        $cart = session()->get('cart');
        unset($cart[$id]);
        session()->put('cart', $cart);
        return response()->json($cart);
    }

    public function deleteAllCart(): \Illuminate\Http\JsonResponse
    {
        $cart = [];
        session()->put('cart', $cart);
        return response()->json($cart);
    }

    public function updateCart(Request $request): \Illuminate\Http\JsonResponse
    {
        $cart = session()->get('cart');
        $cart[$request->id . '_' . $request->size]['quantity'] = $request->quantity;
        $cart[$request->id . '_' . $request->size]['total'] = (int)$cart[$request->id . '_' . $request->size]['price'] * (int)$request->quantity;
        session()->put('cart', $cart);
        return response()->json($cart);
    }
}
