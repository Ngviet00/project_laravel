<?php

namespace Database\Seeders;

use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
            'name' => 'admin',
            'email' => 'admin@gmail.com',
            'phone' => '0987456123',
            'password' => '12345678'
        ]);

        $role = Role::create([
            'name' => 'admin'
        ]);

        DB::table('role_user')->insert([
            'role_id' => $role->id,
            'user_id' => $user->id
        ]);
        DB::table('permission_role')->insert([
            ['permission_id' => 1, 'role_id' => $role->id],
            ['permission_id' => 2, 'role_id' => $role->id],
            ['permission_id' => 3, 'role_id' => $role->id],
            ['permission_id' => 4, 'role_id' => $role->id],
            ['permission_id' => 5, 'role_id' => $role->id],
            ['permission_id' => 7, 'role_id' => $role->id],

            ['permission_id' => 8, 'role_id' => $role->id],
            ['permission_id' => 9, 'role_id' => $role->id],
            ['permission_id' => 10, 'role_id' => $role->id],
            ['permission_id' => 11, 'role_id' => $role->id],
            ['permission_id' => 12, 'role_id' => $role->id],
            ['permission_id' => 14, 'role_id' => $role->id],

            ['permission_id' => 15, 'role_id' => $role->id],
            ['permission_id' => 16, 'role_id' => $role->id],
            ['permission_id' => 17, 'role_id' => $role->id],
            ['permission_id' => 18, 'role_id' => $role->id],
            ['permission_id' => 19, 'role_id' => $role->id],
            ['permission_id' => 21, 'role_id' => $role->id],

            ['permission_id' => 22, 'role_id' => $role->id],
            ['permission_id' => 23, 'role_id' => $role->id],
            ['permission_id' => 24, 'role_id' => $role->id],
            ['permission_id' => 25, 'role_id' => $role->id],
            ['permission_id' => 26, 'role_id' => $role->id],
            ['permission_id' => 28, 'role_id' => $role->id],
        ]);
    }
}
