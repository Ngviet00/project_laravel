<h2 class="text-6xl text-center py-3">Thương hiệu</h2>
<div class="bg-white">
    <div class="max-w-2xl mx-auto py-16 px-4 sm:py-24 sm:px-6 lg:max-w-7xl lg:px-8">
        <h2 class="sr-only">Products</h2>

        <div class="grid grid-cols-1 gap-y-10 sm:grid-cols-2 gap-x-6 lg:grid-cols-4 xl:grid-cols-3 xl:gap-x-8">
            @foreach($brands as $brand)
                <a href="/filter_product?brands={{$brand->id}}" class="group">
                    <div
                        class="w-full aspect-w-1 aspect-h-1 bg-gray-200 rounded-lg overflow-hidden xl:aspect-w-7 xl:aspect-h-8">
                        <img
                            style="height: 254px;"
                            src="/uploads/products/{{$brand->image}}"
                            alt="Tall slender porcelain bottle with natural clay textured body and cork stopper."
                            class="w-full h-full object-center object-cover group-hover:opacity-75">
                    </div>
                    <p class="mt-4 text-lg text-center font-medium text-gray-900">- {{$brand->name}} -</p>
                </a>
            @endforeach
        </div>
    </div>
</div>
