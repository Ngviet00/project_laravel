@extends('layouts.app_client')
@section('content')
    @include('component.slide')
    @include('component.benefit')
    <div class="container">
        @include('component.trending')
        @include('component.latest')
        @include('component.brand')
        @include('component.blog')
    </div>
@endsection
