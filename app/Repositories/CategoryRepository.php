<?php

namespace App\Repositories;

use App\Models\Category;

class CategoryRepository extends BaseRepository
{
    public function model()
    {
        return Category::class;
    }

    public function search($dataSearch)
    {
        return $this->model
            ->withName($dataSearch['name'])
            ->latest('id')
            ->paginate(6);
    }

    public function getParentCategories()
    {
        return $this->model
            ->whereNull('parent_id')
            ->get();
    }
}
