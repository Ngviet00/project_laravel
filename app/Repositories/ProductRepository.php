<?php

namespace App\Repositories;

use App\Models\Product;

class ProductRepository extends BaseRepository
{
    public function model()
    {
        return Product::class;
    }

    public function all()
    {
        return $this->model
            ->with('categories')
            ->paginate(7);
    }

    /**
     * @param $dataSearch
     * @return mixed
     */
    public function search($dataSearch): mixed
    {
        return $this->model
            ->withName($dataSearch['name'])
            ->withMinPrice($dataSearch['min_price'])
            ->withMaxPrice($dataSearch['max_price'])
            ->withCategoryId($dataSearch['category_id'])
            ->latest('id')
            ->paginate(6);
    }
}
