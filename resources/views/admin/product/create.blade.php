@extends('admin.layouts.app')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card my-4">
                    <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
                        <div class="bg-gradient-primary shadow-primary border-radius-lg pt-4 pb-3">
                            <h6 class="text-white text-capitalize ps-3">Create Product</h6>
                        </div>
                    </div>
                    <div class="px-0 pb-2">
                        <form action="{{route('products.store')}}" method="POST" id="form-create-product"
                              enctype="multipart/form-data">
                            @csrf
                            <div class="col-xs-12 col-sm-12 col-md-12 mt-3">
                                <div class="form-group custom-form-group-create">
                                    <strong class="form-check">Name: </strong>
                                    <div class="form-check input-group input-group-dynamic info-horizontal">
                                        <input type="text" name="name" id="name" class="form-control shadow-none"
                                               style="outline: none" value="{{old('name')}}" onfocus="focused(this)"
                                               onfocusout="defocused(this)">
                                    </div>
                                    <div class="text-danger my-1 name_error c_errors"></div>
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-12 col-md-12 mt-3">
                                <div class="form-group custom-form-group-create">
                                    <strong class="form-check">Price: </strong>
                                    <div class="form-check input-group input-group-dynamic info-horizontal">
                                        <input type="text" name="price" id="price" class="form-control shadow-none"
                                               style="outline: none" value="{{old('price')}}" onfocus="focused(this)"
                                               onfocusout="defocused(this)">
                                    </div>
                                    <div class="text-danger my-1 price_error c_errors"></div>
                                </div>
                            </div>
                            <textarea class="form-control" name="desc" id="short_desc" placeholder="Mo ta ngan"></textarea>
                            <div class="col-xs-12 col-sm-12 col-md-12 mt-3">
                                <div class="form-group custom-form-group-create">
                                    <strong class="form-check">Image: </strong>

                                    <div class="form-check input-group input-group-dynamic info-horizontal">
                                        <input class="form-control form-control" name="image" type="file" id="image">
                                    </div>
                                    <div class="text-danger my-1 image_error c_errors"></div>
                                </div>
                            </div>
                            <div>
                                <img src="" alt="" id="showImage">
                            </div>

                            <div class="form-group" style="padding-left: 20px;">
                                <label for="" class="text-lg text-bold text-dark mt-2">Category</label>
                                <select class="form-select" name="category_id">
                                    <option selected disabled>Open this select menu</option>
                                    @foreach($categories as $category)
                                        <option value="{{$category->id}}">{{$category->name}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group" style="padding-left: 20px;">
                                <label for="" class="text-lg text-bold text-dark mt-2">Brand</label>
                                <select class="form-select" name="brand_id">
                                    <option selected disabled>Open this select menu</option>
                                    @foreach($brands as $item)
                                        <option value="{{$item->id}}">{{$item->name}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group d-flex mt-3" style="padding-left: 20px;">
                                <label for="" class="text-lg text-bold text-dark mt-2">Size: </label><br>
                                @foreach($sizes as $size)
                                    <div class="form-check d-flex align-items-center">
                                        <input type="checkbox" value="{{ $size->id }}" name="size_id[]"
                                               id="size-{{$size->id}}"
                                               class="form-input "
                                        >
                                        <label for="size-{{$size->id}}" class="m-0 text-dark"
                                               style="display:block;margin-left:5px !important;font-size:18px">
                                            {{ $size->size }}
                                        </label>
                                    </div>
                                @endforeach
                            </div>
                            <div class="form-group" style="padding-left: 20px;">
                                <label for="" class="text-lg text-bold text-dark mt-2">List thumbnail</label>
                                <br><input type="file" name="thumbnails[]" multiple>
                            </div>
                            <button type="submit" class="btn bg-gradient-primary btn-store-product"
                                    style="margin-left: 20px;margin-top: 20px;">Add
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        CKEDITOR.replace( 'short_desc' );
    </script>
@endsection
