@extends('admin.layouts.app')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card my-4">
                    <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
                        <div class="bg-gradient-primary shadow-primary border-radius-lg pt-4 pb-3">
                            <h6 class="text-white text-capitalize ps-3">Create Slide</h6>
                        </div>
                    </div>
                    <div class="px-0 pb-2">
                        <form method="POST" action="{{route('blogs.update', $blog->id)}}" class="p-4"
                              enctype="multipart/form-data">
                            @csrf
                            @method('PUT')
                            <div class="form-group">
                                <label for="title">Tiêu đề</label>
                                <input type="text" class="form-control" name="title" id="title" value="{{$blog->title}}"
                                       placeholder="Title">
                            </div>
                            <div class="form-group">
                                <label for="image">Hinh anh</label><br>
                                <input type="file" name="image" id="image" onchange="loadFile(event)">
                            </div>
                            <div class="preview-image mt-3">
                                <img id="output" style="width: 25rem" src="/uploads/products/{{$blog->image}}"/>
                            </div>
                            <div class="form-group">
                                <label for="short_desc">Mo ta ngan</label>
                                <textarea class="form-control" name="short_desc" id="short_desc"
                                          placeholder="Mo ta ngan">{{$blog->short_desc}}</textarea>
                            </div>
                            <div class="form-group">
                                <label for="short_desc">Mo ta chi tiet</label>
                                <textarea class="form-control" name="desc_detail" id="desc_detail"
                                          placeholder="Mo ta chi tiet">{{$blog->description}}</textarea>
                            </div>
                            <a href="{{route('blogs.index')}}" class="mt-4 btn btn-dark">Back</a>
                            <button type="submit" class=" mt-4 btn btn-danger">Update</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="//cdn.ckeditor.com/4.16.2/standard/ckeditor.js"></script>
    <script>
        CKEDITOR.replace('desc_detail');
        let loadFile = function (event) {
            let reader = new FileReader();
            reader.onload = function () {
                let output = document.getElementById('output');
                output.src = reader.result;
            };
            reader.readAsDataURL(event.target.files[0]);
        };
    </script>
@endsection
@section('js')
    <script src="/js/admin/blog.js"></script>
@endsection
