@extends('admin.layouts.app')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card my-4">
                    <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
                        <div class="bg-gradient-primary shadow-primary border-radius-lg pt-4 pb-3">
                            <h6 class="text-white text-capitalize ps-3">Show Product</h6>
                        </div>
                    </div>

                    <div class="p-4">
                        <label for="name" class="text-lg text-bold d-block text-dark">ID: {{ $role->id }}</label>
                        <label for="name" class="text-lg text-bold d-block text-dark">Name: {{ $role->name }}</label>
                        <label for="name" class="text-lg text-bold d-block text-dark">
                            Permissions:
                            <div class="d-flex flex-wrap">
                                @foreach($role->permissions as $key => $permission)
                                    <span class="badge badge-sm bg-gradient-success text-capitalize my-1"
                                          style="margin-right: 3px;">
                                    {{ $permission->name }}
                                </span>
                                @endforeach
                            </div>
                        </label>
                        <a href="{{ route('roles.index') }}" class="btn btn-dark">Back</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
