<?php

namespace App\Repositories;

use App\Models\Brand;
use JasonGuru\LaravelMakeRepository\Repository\BaseRepository;

class BrandRepository extends BaseRepository
{
    /**
     * @return string
     *  Return the model
     */
    public function model()
    {
        return Brand::class;
    }
}
