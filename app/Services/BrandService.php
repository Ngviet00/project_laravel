<?php

namespace App\Services;

use App\Repositories\BlogRepository;
use App\Repositories\BrandRepository;
use App\Traits\HandleImage;

/**
 * Class BrandService
 * @package App\Services
 */
class BrandService
{
    use HandleImage;

    protected $brandRepository;

    public function __construct(BrandRepository $brandRepository)
    {
        $this->brandRepository = $brandRepository;
    }

    public function all()
    {
        return $this->brandRepository->all();
    }

    public function create($request)
    {
        $data['image'] = $this->saveImage($request);
        $data['name'] = $request->name;
        return $this->brandRepository->create($data);
    }

    public function findOrFail($id)
    {
        return $this->brandRepository->getById($id);
    }

    public function update($request, $id)
    {
        $dataUpdate = $request->all();
        $brand = $this->brandRepository->getById($id);
        $dataUpdate['image'] = $this->updateImage($request, $brand->image);
        $dataUpdate['name'] = $request->name;
        $brand->update($dataUpdate);
        return $brand;
    }

    public function delete($id)
    {
        $brand = $this->brandRepository->getById($id);

        $this->deleteImage($brand->image);

        $brand->delete();

        return $brand;
    }
}
