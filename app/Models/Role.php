<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    use HasFactory;

    protected $fillable = ['name'];

    public function users()
    {
        return $this->belongsToMany(User::class);
    }

    public function permissions()
    {
        return $this->belongsToMany(Permission::class);
    }

    public function scopeWithName($query, $name)
    {
        return $name ? $query->where('name', 'like', "%" . $name . "%") : null;
    }

    public function hasPermission($permissionName)
    {
        return $this->permissions->contains('name', $permissionName);
    }

    public function attachPermission($permissionId)
    {
        return $this->permissions()->attach($permissionId);
    }

    public function syncPermission($permissionId)
    {
        return $this->permissions()->sync($permissionId);
    }

    public function detachPermission()
    {
        return $this->permissions()->detach();
    }
}
