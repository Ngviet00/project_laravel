<?php

namespace Tests\Feature\Categories;

use Illuminate\Http\Response;
use Tests\TestCase;

class GetListCategoryTest extends TestCase
{
    /** @test */
    public function unauthenticated_can_not_see_list_category()
    {
        $response = $this->get($this->getListCategoryRoute());
        $response->assertRedirect(route('login'));
    }

    /** @test */
    public function authenticated_with_permission_can_see_list_category()
    {
        $this->loginUserWithPermission('view_category');
        $response = $this->get($this->getListCategoryRoute());
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('admin.category.index');
    }

    /** @test */
    public function authenticated_without_permission_can_not_see_list_category()
    {
        $this->loginUser();
        $response = $this->get($this->getListCategoryRoute());
        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    public function getListCategoryRoute()
    {
        return route('categories.index');
    }
}
