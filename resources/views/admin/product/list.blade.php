@php
    $stt = (($_GET['page'] ?? 1) - 1) * 6
@endphp
<div class="px-0 pb-2">
    <div class="table-responsive p-0">
        <table class="table align-items-center mb-0">
            <thead>
            <tr>
                <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                    ID
                </th>
                <th
                    class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
                    Name
                </th>
                <th
                    class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                    Image
                </th>
                <th
                    class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                    Category
                </th>
                <th
                    class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                    Price
                </th>
                <th
                    class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                    Action
                </th>
            </tr>
            </thead>
            <tbody>
            @foreach($products->load('categories') as $product)
                <tr>
                    <td>
                        <p style="padding-left: 20px" class="text-sm font-weight-bold mb-0">
                            {{ ++$stt }}</p>
                    </td>
                    <td>
                        <p class="text-sm font-weight-bold mb-0">{{ $product['name'] }}</p>
                    </td>
                    <td>
                        <p class="text-sm font-weight-bold mb-0 text-center">
                            <img src="/uploads/products/{{ $product['image'] }}" alt=""
                                 class="text-center" width="100" height="70">
                        </p>
                    </td>
                    <td class="align-middle text-center">
                        <span class="text-secondary text-sm font-weight-bold">
                            <ul>
                                <li class="list-unstyled">{{$product->categories ? $product->categories->name : ''}}</li>
                            </ul>
                        </span>
                    </td>
                    <td>
                        <p class="text-sm font-weight-bold mb-0 text-center">
                            {{$product['price']}}
                        </p>
                    </td>
                    <td class="align-middle d-flex justify-content-around align-items-center pt-3"
                        style="height: 88px;">

                        @hasPermission('edit_product')
                        <a href="{{ route('products.show', $product['id']) }}"><i class="fas fa-edit"></i></a>
                        @endhasPermission

                        @hasPermission('delete_product')
                        <button type="button"
                                class='btn m-0 text-sm btn-delete-product'
                                data-action="{{route('products.delete', $product['id'])}}">
                            <i class="fas fa-trash-alt text-secondary"></i>
                        </button>
                        @endhasPermission
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        @if(count($products) == 0)
            <div class="py-3" style="margin-left: 40%;">
                <p style="margin-left: 22px !important;" class="m-0 fw-bold text-danger">No Data</p>
            </div>
        @endif
        <div class="d-flex justify-content-center">
            {{ $products->appends(request()->all())->links() }}
        </div>
    </div>
</div>
