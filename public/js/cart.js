$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

function addToCart() {
    let data = {
        'id': $('#id_product').val(),
        'name': $('#name_product').html(),
        'brand': $('#brand_product').html(),
        'quantity': $('#quantity_product').val(),
        'price': $('#price_product').text(),
        'image': $('#image_product').attr('src'),
        'size': $('.active_radio_checkbox').attr('data-size'),
    }
    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: '/addToCart',
        data: data,
        success: function (response) {
            window.location.href = '/cart';
        },
        error: function (err) {
            console.log(err);
        }
    });
}

function chooseRadio(id) {
    $('.radio_size').removeClass('active_radio_checkbox');
    $('#radio_button_' + id).addClass('active_radio_checkbox');
}

$('.btn_delete').click(function () {
    let id = $(this).attr('id') + '_' + $(this).attr('size');
    let check = confirm("Bạn có muốn xóa sản phẩm này không?");
    if (check) {
        $.ajax({
            url: 'deleteCart/' + id,
            method: "get",
            success: function (response) {
                window.location.reload();
            },
            error: function (err) {
                alert('Xóa thất bại!');
            }
        });
    }
});

$('.delete_all_cart').click(function () {
    let check = confirm("Bạn có muốn xóa tất cả sản phẩm trong giỏ hàng không?");
    if (check) {
        $.ajax({
            url: 'deleteAllCart',
            method: "get",
            success: function (response) {
                window.location.reload();
            },
            error: function (err) {
                alert('Xóa thất bại!');
            }
        });
    }
})

function inputIncrement(id, size) {
    let idSize = Number($('#qty-' + id + '-' + size).val());
    idSize++;
    $('#qty-' + id + '-' + size).val(idSize);
    let data = {
        'id': id,
        'quantity': idSize,
        'size': size,
    }
    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: '/updateCart',
        data: data,
        success: function (response) {
            window.location.reload();
        },
        error: function (err) {
            console.log(err);
        }
    });
}

function inputDecrement(id, size) {
    let idSize = Number($('#qty-' + id + '-' + size).val());
    if (idSize > 1) {
        idSize--;
        $('#qty-' + id + '-' + size).val(idSize);
        let data = {
            'id': id,
            'quantity': idSize,
            'size': size,
        }
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: '/updateCart',
            data: data,
            success: function (response) {
                window.location.reload();
            },
            error: function (err) {
                console.log(err);
            }
        });
    }
}
