<?php

namespace App\Http\Composers;

use App\Repositories\CategoryRepository;
use Illuminate\View\View;

class CategoryComposer
{

    protected $categoryRepository;

    public function __construct(CategoryRepository $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    public function compose(View $view)
    {
        $view->with('countCategory', $this->categoryRepository->count());
        $view->with('categories', $this->categoryRepository->all());
        $view->with('parentCategory', $this->categoryRepository->getParentCategories());
    }
}
