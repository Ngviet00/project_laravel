<?php

namespace App\Http\Requests\User;

use App\Rules\PhoneNumberVnRule;
use Illuminate\Foundation\Http\FormRequest;

class UpdateUserRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'required',
            'email' => 'required|email|unique:users,email,' . $this->user->id,
            'phone' => ['required', new PhoneNumberVnRule()],
            'password' => 'required'
        ];
    }
}
