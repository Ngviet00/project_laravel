<?php

use App\Http\Controllers\SlideController;
use Illuminate\Support\Facades\Route;

Route::name('slides.')->prefix('slides')->group(function () {
    Route::get('/', [SlideController::class, 'index'])
        ->name('index');

    Route::get('/create', [SlideController::class, 'create'])
        ->name('create');

    Route::post('/', [SlideController::class, 'store'])
        ->name('store');

    Route::get('/{id}', [SlideController::class, 'show'])
        ->name('show');

    Route::get('/edit/{id}', [SlideController::class, 'edit'])
        ->name('edit');

    Route::put('/edit/{id}', [SlideController::class, 'update'])
        ->name('update');

    Route::delete('/{id}', [SlideController::class, 'destroy'])
        ->name('delete');
});
