<?php

namespace Tests\Feature\Users;

use App\Models\User;
use Illuminate\Http\Response;
use Tests\TestCase;

class DeleteUserTest extends TestCase
{
    /** @test */
    public function unauthenticated_can_not_delete_user()
    {
        $userCreated = $this->createFactoryUser();;
        $response = $this->delete($this->deleteUserRoute($userCreated['id']));
        $response->assertRedirect(route('login'));
    }

    /** @test */
    public function authenticated_with_permission_can_delete_user()
    {
        $this->loginUserWithPermission('delete_user');
        $userCreated = $this->createFactoryUser();;
        $response = $this->delete($this->deleteUserRoute($userCreated['id']));
        $response->assertStatus(Response::HTTP_FOUND);
        $this->assertDatabaseMissing('users', $userCreated);
    }

    /** @test */
    public function authenticated_with_permission_can_not_delete_if_user_not_exists()
    {
        $this->loginUserWithPermission('delete_user');
        $userId = -1;
        $response = $this->delete($this->deleteUserRoute($userId));
        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }

    /** @test */
    public function authenticated_without_permission_can_not_delete_user()
    {
        $this->loginUser();
        $userCreated = $this->createFactoryUser();
        $response = $this->delete($this->deleteUserRoute($userCreated['id']));
        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    public function deleteUserRoute($id): string
    {
        return route('users.delete', $id);
    }

    public function createFactoryUser(): array
    {
        return User::factory()->create()->toArray();
    }
}
