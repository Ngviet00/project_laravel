<?php

namespace Tests\Feature\Roles;

use App\Models\Role;
use Illuminate\Http\Response;
use Tests\TestCase;

class GetShowRoleTest extends TestCase
{
    /** @test */
    public function unauthenticated_can_not_see_one_role()
    {
        $roleCreated = $this->createFactoryRole();
        $response = $this->get($this->getShowRoleRoute($roleCreated['id']));
        $response->assertRedirect(route('login'));
    }

    /** @test */
    public function authenticated_with_permission_see_one_role()
    {
        $this->loginUserWithPermission('view_role');
        $roleCreated = $this->createFactoryRole();
        $response = $this->get($this->getShowRoleRoute($roleCreated['id']));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('admin.role.show');
        $response->assertSee($roleCreated['name']);
    }

    /** @test */
    public function authenticated_with_permission_can_not_see_one_role_if_not_exists()
    {
        $this->loginUserWithPermission('view_role');
        $roleId = -1;
        $response = $this->get($this->getShowRoleRoute($roleId));
        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }

    /** @test */
    public function authenticated_without_permission_not_see_one_role()
    {
        $this->loginUser();
        $roleCreated = $this->createFactoryRole();
        $response = $this->get($this->getShowRoleRoute($roleCreated['id']));
        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    public function getShowRoleRoute($id): string
    {
        return route('roles.show', $id);
    }

    public function createFactoryRole(): array
    {
        return Role::factory()->create()->toArray();
    }
}
