<?php

namespace Tests\Feature\Categories;

use App\Models\Category;
use Illuminate\Http\Response;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class CreateCategoryTest extends TestCase
{
    /** @test */
    public function authenticated_with_permission_can_create_category()
    {
        $this->loginUserWithPermission('store_category');
        $dataCreate = $this->makeFactoryCategory();
        $response = $this->postJson($this->storeCategoryRoute(), $dataCreate);
        $response->assertStatus(Response::HTTP_OK);
        $response
            ->assertJson(fn(AssertableJson $json) => $json->where('status_code', Response::HTTP_OK)
                ->where('message', 'Create Category Success')
                ->etc()
            );
        $this->assertDatabaseHas('categories', [
            'name' => $dataCreate['name']
        ]);
    }

    /** @test */
    public function authenticated_with_permission_can_not_create_category_if_name_is_null()
    {
        $this->loginUserWithPermission('store_category');
        $dataCreate = Category::factory()->make([
            'name' => null
        ])->toArray();
        $response = $this->postJson($this->storeCategoryRoute(), $dataCreate);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertJsonValidationErrors(['name']);
    }

    /** @test */
    public function authenticated_without_permission_can_not_create_category()
    {
        $this->loginUser();
        $dataCreate = $this->makeFactoryCategory();
        $response = $this->post($this->storeCategoryRoute(), $dataCreate);
        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    /** @test */
    public function unauthenticated_can_not_create_category()
    {
        $dataCreate = $this->makeFactoryCategory();
        $response = $this->post($this->storeCategoryRoute(), $dataCreate);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect(route('login'));
    }

    public function storeCategoryRoute()
    {
        return route('categories.store');
    }

    public function makeFactoryCategory()
    {
        return Category::factory()->make()->toArray();
    }
}
