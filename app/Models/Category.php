<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;


class Category extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'slug', 'parent_id'];

    public function products()
    {
        return $this->hasMany(Product::class);
    }

    public function parentCategory()
    {
        return $this->belongsTo(Category::class, 'parent_id', 'id');
    }

    public function childrenCategory()
    {
        return $this->hasMany(Category::class, 'parent_id', 'id');
    }

    public function scopeWithName($query, $name)
    {
        return $name ? $query->where('name', 'like', "%" . $name . "%") : null;
    }

    public function setNameAttribute($name)
    {
        $this->attributes['name'] = $name;
    }

    public function setSlugAttribute()
    {
        $this->attributes['slug'] = Str::slug($this->attributes['name']);
    }
}
