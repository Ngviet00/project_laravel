<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            PermissionSeeder::class,
            SuperAdminSeeder::class,
            AdminSeeder::class,
            CategorySeeder::class,
            RoleSeeder::class,
            SizeSeeder::class
//            ProductSeeder::class
        ]);
    }
}
