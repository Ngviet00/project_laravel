<?php

namespace Tests\Feature\Users;

use App\Models\User;
use Illuminate\Http\Response;
use Tests\TestCase;

class GetListUserTest extends TestCase
{
    /** @test */
    public function unauthenticated_can_not_get_list_user()
    {
        $response = $this->get($this->getListUserRoute());
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect(route('login'));
    }

    /** @test */
    public function authenticated_with_permission_can_get_list_user()
    {
        $this->loginUserWithPermission('view_user');
        $response = $this->get($this->getListUserRoute());
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('admin.user.index');
    }

    /** @test */
    public function authenticated_without_permission_can_not_get_list_user()
    {
        $this->loginUser();
        $response = $this->get($this->getListUserRoute());
        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    protected function getListUserRoute()
    {
        return route('users.index');
    }
}
