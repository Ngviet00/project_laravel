<?php

namespace Tests\Feature\Users;

use App\Models\User;
use Illuminate\Http\Response;
use Tests\TestCase;

class CreateUserTest extends TestCase
{
    /** @test */
    public function unauthenticated_can_not_view_form_create_user()
    {
        $response = $this->get($this->getCreateUserRoute());
        $response->assertRedirect(route('login'));
    }

    /** @test */
    public function authenticated_without_permission_can_not_view_form_create_user()
    {
        $this->loginUser();
        $response = $this->get($this->getCreateUserRoute());
        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    /** @test */
    public function authenticated_with_permission_can_view_form_create_user()
    {
        $this->loginUserWithPermission('view_user');
        $response = $this->get($this->getCreateUserRoute());
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('admin.user.create');
    }

    /** @test */
    public function authenticated_without_permission_can_not_create_new_user()
    {
        $this->loginUser();
        $dataCreate = $this->makeFactoryUser();
        $response = $this->post($this->storeUserRoute(), $dataCreate);
        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    /** @test */
    public function authenticated_with_permission_can_not_create_new_user_if_name_is_null()
    {
        $this->loginUserWithPermission('store_user');
        $dataCreate = $this->makeFactoryUserSetFieldNull('name');
        $response = $this->post($this->storeUserRoute(), $dataCreate);
        $response->assertSessionHasErrors(['name']);
    }

    /** @test */
    public function authenticated_with_permission_can_not_create_new_user_if_email_is_null()
    {
        $this->loginUserWithPermission('store_user');
        $dataCreate = $this->makeFactoryUserSetFieldNull('email');
        $response = $this->post($this->storeUserRoute(), $dataCreate);
        $response->assertSessionHasErrors(['email']);
    }

    /** @test */
    public function authenticated_with_permission_can_not_create_new_user_if_email_is_invalid()
    {
        $this->loginUserWithPermission('store_user');
        $dataCreate = User::factory()->make([
            'email' => 'test123.com'
        ])->toArray();
        $response = $this->post($this->storeUserRoute(), $dataCreate);
        $response->assertSessionHasErrors(['email']);
    }

    /** @test */
    public function authenticated_with_permission_can_not_create_new_user_if_phone_is_null()
    {
        $this->loginUserWithPermission('store_user');
        $dataCreate = $this->makeFactoryUserSetFieldNull('phone');
        $response = $this->post($this->storeUserRoute(), $dataCreate);
        $response->assertSessionHasErrors(['phone']);
    }

    /** @test */
    public function authenticated_with_permission_can_not_create_new_user_if_phone_is_invalid()
    {
        $this->loginUserWithPermission('store_user');
        $dataCreate = User::factory()->make([
            'phone' => '311265975'
        ])->toArray();
        $response = $this->post($this->storeUserRoute(), $dataCreate);
        $response->assertSessionHasErrors(['phone']);
    }

    /** @test */
    public function authenticated_with_permission_can_not_create_new_user_if_password_is_null()
    {
        $this->loginUserWithPermission('store_user');
        $dataCreate = $this->makeFactoryUserSetFieldNull('password');
        $response = $this->post($this->storeUserRoute(), $dataCreate);
        $response->assertSessionHasErrors(['password']);
    }

    /** @test */
    public function authenticated_with_permission_can_see_name_required_text_if_name_is_null()
    {
        $this->loginUserWithPermission('store_user');
        $dataCreate = $this->makeFactoryUserSetFieldNull('name');
        $response = $this->from($this->getCreateUserRoute())->post($this->storeUserRoute(), $dataCreate);
        $response->assertSessionHasErrors(['name']);
    }

    /** @test */
    public function authenticated_with_permission_can_see_email_required_text_if_email_is_null()
    {
        $this->loginUserWithPermission('store_user');
        $dataCreate = $this->makeFactoryUserSetFieldNull('email');
        $response = $this->from($this->getCreateUserRoute())->post($this->storeUserRoute(), $dataCreate);
        $response->assertSessionHasErrors(['email']);
    }

    /** @test */
    public function authenticated_with_permission_can_see_phone_required_text_if_phone_is_null()
    {
        $this->loginUserWithPermission('store_user');
        $dataCreate = $this->makeFactoryUserSetFieldNull('phone');
        $response = $this->from($this->getCreateUserRoute())->post($this->storeUserRoute(), $dataCreate);
        $response->assertSessionHasErrors(['phone']);
    }

    /** @test */
    public function authenticated_with_permission_can_see_password_required_text_if_password_is_null()
    {
        $this->loginUserWithPermission('store_user');
        $dataCreate = $this->makeFactoryUserSetFieldNull('password');
        $response = $this->from($this->getCreateUserRoute())->post($this->storeUserRoute(), $dataCreate);
        $response->assertSessionHasErrors(['password']);
    }

    /** @test */
    public function authenticated_with_permission_can_create_new_user_if_data_is_valid()
    {
        $this->loginUserWithPermission('store_user');
        $dataCreate = $this->makeFactoryUser();
        $dataCreate['password'] = $this->faker->password(8);
        $response = $this->post($this->storeUserRoute(), $dataCreate);
        $response->assertStatus(Response::HTTP_FOUND);
        $this->assertDatabaseHas('users', [
            'name' => $dataCreate['name'],
            'email' => $dataCreate['email']
        ]);
        $response->assertRedirect(route('users.index'));
    }

    public function getCreateUserRoute(): string
    {
        return route('users.create');
    }

    public function storeUserRoute(): string
    {
        return route('users.store');
    }

    public function makeFactoryUser(): array
    {
        return User::factory()->make()->toArray();
    }

    public function makeFactoryUserSetFieldNull($field): array
    {
        return User::factory()->make([
            $field => null
        ])->toArray();
    }
}
