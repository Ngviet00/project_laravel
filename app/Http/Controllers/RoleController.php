<?php

namespace App\Http\Controllers;

use App\Http\Requests\Role\CreateRoleRequest;
use App\Http\Requests\Role\UpdateRoleRequest;
use App\Services\RoleService;
use Illuminate\Http\Request;

class RoleController extends Controller
{

    protected $roleService;

    public function __construct(RoleService $roleService)
    {
        $this->roleService = $roleService;
    }

    public function index(Request $request)
    {
        $roles = $this->roleService->search($request);
        return view('admin.role.index', compact('roles'));
    }

    public function create()
    {
        return view('admin.role.create');
    }

    public function store(CreateRoleRequest $request)
    {
        $this->roleService->create($request);
        return redirect()->route('roles.index')->with('message', 'Create Role Success');
    }

    public function show($id)
    {
        $role = $this->roleService->find($id);
        return view('admin.role.show', compact('role'));
    }

    public function edit($id)
    {
        $role = $this->roleService->find($id);
        return view('admin.role.edit', compact('role'));
    }

    public function update(UpdateRoleRequest $request, $id)
    {
        $this->roleService->update($request, $id);
        return redirect()->route('roles.index')->with('message', 'Update Role Success');
    }

    public function destroy($id)
    {
        $this->roleService->delete($id);
        return redirect()->route('roles.index')->with('message', 'Delete Role Success');
    }
}
