<?php

namespace Tests\Feature\Roles;

use App\Models\Role;
use Illuminate\Http\Response;
use Tests\TestCase;

class GetListRoleTest extends TestCase
{
    /** @test */
    public function unauthenticated_can_not_see_list_role()
    {
        $response = $this->get($this->getListRoleRoute());
        $response->assertRedirect(route('login'));
    }

    /** @test */
    public function authenticated_with_permission_see_list_role()
    {
        $this->loginUserWithPermission('view_role');
        $roleCreated = Role::factory()->create()->toArray();
        $response = $this->get($this->getListRoleRoute());
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('admin.role.index');
        $response->assertSee($roleCreated['name']);
    }

    /** @test */
    public function authenticated_without_permission_not_see_list_role()
    {
        $this->loginUser();
        $response = $this->get($this->getListRoleRoute());
        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    public function getListRoleRoute()
    {
        return route('roles.index');
    }
}
