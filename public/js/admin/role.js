$(document).ready(function () {
    $('.select_all').on('change', function () {
        $('.checkbox').prop('checked', $(this).prop("checked"));
    });

    if ($('.checkbox:checked').length == $('.checkbox').length) {
        $('.select_all').prop('checked', true);
    }

    $('.checkbox').change(function () {
        if ($('.checkbox:checked').length == $('.checkbox').length) {
            $('.select_all').prop('checked', true);
        } else {
            $('.select_all').prop('checked', false);
        }
    });
});