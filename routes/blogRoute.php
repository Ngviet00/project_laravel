<?php

use App\Http\Controllers\BlogController;
use Illuminate\Support\Facades\Route;

Route::name('blogs.')->prefix('blogs')->group(function () {
    Route::get('/', [BlogController::class, 'index'])
        ->name('index');

    Route::get('/create', [BlogController::class, 'create'])
        ->name('create');

    Route::post('/', [BlogController::class, 'store'])
        ->name('store');

    Route::get('/{id}', [BlogController::class, 'show'])
        ->name('show');

    Route::get('/edit/{id}', [BlogController::class, 'edit'])
        ->name('edit');

    Route::put('/edit/{id}', [BlogController::class, 'update'])
        ->name('update');

    Route::delete('/{id}', [BlogController::class, 'destroy'])
        ->name('delete');
});
