<?php

namespace App\Services;

use App\Repositories\SlideRepository;
use App\Traits\HandleImage;

class SlideService
{
    use HandleImage;

    protected SlideRepository $slideRepository;

    public function __construct(SlideRepository $slideRepository)
    {
        $this->slideRepository = $slideRepository;
    }

    public function all()
    {
        return $this->slideRepository->all();
    }

    public function create($request)
    {
        $data['name'] = $this->saveImage($request);
        return $this->slideRepository->create($data);
    }

    public function findOrFail($id)
    {
        return $this->slideRepository->getById($id);
    }

    public function update($request, $id)
    {
        $dataUpdate = $request->all();

        $slide = $this->slideRepository->getById($id);

        $dataUpdate['name'] = $this->updateImage($request, $slide->name);

        $slide->update($dataUpdate);

        return $slide;
    }

    public function delete($id)
    {
        $slide = $this->slideRepository->getById($id);

        $this->deleteImage($slide->name);

        $slide->delete();

        return $slide;
    }
}
