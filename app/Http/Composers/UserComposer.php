<?php

namespace App\Http\Composers;

use App\Repositories\UserRepository;
use Illuminate\View\View;

class UserComposer
{

    protected UserRepository $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function compose(View $view)
    {
        $view->with('countUser', $this->userRepository->count());
    }
}
