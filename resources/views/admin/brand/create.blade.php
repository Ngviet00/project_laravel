@extends('admin.layouts.app')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card my-4">
                    <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
                        <div class="bg-gradient-primary shadow-primary border-radius-lg pt-4 pb-3">
                            <h6 class="text-white text-capitalize ps-3">Create Brand</h6>
                        </div>
                    </div>
                    <div class="px-0 pb-2">
                        <form method="POST" action="{{route('brands.store')}}" class="p-4"
                              enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                <label for="title">Name</label>
                                <input type="text" class="form-control" name="name" id="name" placeholder="Name">
                            </div>
                            <div class="form-group">
                                <label for="image">Image:</label><br>
                                <input type="file" name="image" id="image" onchange="loadFile(event)">
                            </div>
                            <div class="preview-image mt-3">
                                <img id="output" style="width: 25rem"/>
                            </div>
                            <a href="{{route('brands.index')}}" class="mt-4 btn btn-dark">Back</a>
                            <button type="submit" class=" mt-4 btn btn-danger">Add</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        let loadFile = function (event) {
            let reader = new FileReader();
            reader.onload = function () {
                let output = document.getElementById('output');
                output.src = reader.result;
            };
            reader.readAsDataURL(event.target.files[0]);
        };
    </script>
@endsection
@section('js')
    <script src="/js/admin/brand.js"></script>
@endsection
