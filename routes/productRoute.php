<?php

use App\Http\Controllers\ProductController;
use Illuminate\Support\Facades\Route;

Route::name('products.')->prefix('products')->group(function () {
    Route::get('/', [ProductController::class, 'index'])
        ->name('index')
        ->middleware('permission:view_product');

    Route::get('/list', [ProductController::class, 'list'])
        ->name('list')
        ->middleware('permission:view_product');

    Route::get('/create', [ProductController::class, 'create'])
        ->name('create')
        ->middleware('permission:create_product');
    Route::post('/', [ProductController::class, 'store'])
        ->name('store')
        ->middleware('permission:store_product');

    Route::get('/{id}', [ProductController::class, 'show'])
        ->name('show')
        ->middleware('permission:show_product');

    Route::get('/edit/{id}', [ProductController::class, 'edit'])
        ->name('edit')
        ->middleware('permission:edit_product');

    Route::post('/{id}', [ProductController::class, 'update'])
        ->name('update')
        ->middleware('permission:update_product');

    Route::delete('/{id}', [ProductController::class, 'destroy'])
        ->name('delete')
        ->middleware('permission:delete_product');
});
