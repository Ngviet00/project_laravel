@extends('admin.layouts.app')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card my-4">
                    <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
                        <div class="bg-gradient-primary shadow-primary border-radius-lg pt-4 pb-3">
                            <h6 class="text-white text-capitalize ps-3">Create Slide</h6>
                        </div>
                    </div>

                    <div class="px-0 pb-2">
                        <form method="POST" action="{{route('slides.store')}}" class="p-4" enctype="multipart/form-data">
                            @csrf
                            <div class="col-xs-12 col-sm-12 col-md-12 mt-3">
                                <div class="form-group custom-form-group-create">
                                    <strong class="form-check p-0 mb-3">Image: </strong>
                                    <input type="file" name="image" id="image" onchange="loadFile(event)">
                                    @error('image')
                                        <div class="text-danger my-1">{{ $message }} </div>
                                    @enderror
                                </div>
                                <div class="preview-image mt-3">
                                    <img id="output" style="width: 45rem"/>
                                </div>
                            </div>
                            <a href="{{route('slides.index')}}" class="mt-4 btn btn-dark">Back</a>
                            <button type="submit" class=" mt-4 btn btn-danger">Add</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        let loadFile = function(event) {
            let reader = new FileReader();
            reader.onload = function(){
                let output = document.getElementById('output');
                output.src = reader.result;
            };
            reader.readAsDataURL(event.target.files[0]);
        };
    </script>
@endsection
@section('js')
    <script src="/js/admin/role.js"></script>
@endsection
