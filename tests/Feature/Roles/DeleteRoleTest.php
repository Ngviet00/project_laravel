<?php

namespace Tests\Feature\Roles;

use App\Models\Role;
use Illuminate\Http\Response;
use Tests\TestCase;

class DeleteRoleTest extends TestCase
{
    /** @test */
    public function unauthenticated_can_not_delete_role()
    {
        $roleCreated = $this->createFactoryRole();
        $response = $this->delete($this->deleteRoleRoute($roleCreated['id']));
        $response->assertRedirect(route('login'));
    }

    /** @test */
    public function authenticated_with_permission_can_delete_role()
    {
        $this->loginUserWithPermission('delete_role');
        $roleCreated = $this->createFactoryRole();
        $response = $this->delete($this->deleteRoleRoute($roleCreated['id']));
        $response->assertStatus(Response::HTTP_FOUND);
        $this->assertDatabaseMissing('roles', $roleCreated);
        $response->assertRedirect(route('roles.index'));
    }

    /** @test */
    public function authenticated_with_permission_can_not_delete_if_role_not_exists()
    {
        $this->loginUserWithPermission('delete_role');
        $roleId = -1;
        $response = $this->delete($this->deleteRoleRoute($roleId));
        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }

    /** @test */
    public function authenticated_without_permission_can_not_delete_role()
    {
        $this->loginUser();
        $roleCreated = $this->createFactoryRole();
        $response = $this->delete($this->deleteRoleRoute($roleCreated['id']));
        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    public function deleteRoleRoute($id): string
    {
        return route('roles.delete', $id);
    }

    public function createFactoryRole(): array
    {
        return Role::factory()->create()->toArray();
    }
}
