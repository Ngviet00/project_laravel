<div class="container" style="max-width: 1170px;margin: 0px auto">
    <div class="flex justify-between py-3 mt-10">
        <div class="benefit text-center" style="flex-basis: 24%">
            <p>
                <i class="fa-solid fa-truck text-3xl"></i>
            </p>
            <h2 class="my-2 text-xl">Miễn phí vận chuyển</h2>
        </div>
        <div class="benefit text-center"
             style="
             flex-basis: 24%;
             border-right: 1px solid #e8e2e2;
             border-left: 1px solid #e8e2e2"
        >
            <p>
                <i class="fa-solid fa-arrow-rotate-left text-3xl"></i>
            </p>
            <h2 class="my-2 text-xl">Return Policy</h2>
        </div>
        <div class="benefit text-center" style="flex-basis: 24%;border-right: 1px solid #e8e2e2;">
            <p>
                <i class="fa-solid fa-headphones text-3xl"></i>
            </p>
            <h2 class="my-2 text-xl">24/7 Hỗ trợ</h2>
        </div>
        <div class="benefit text-center" style="flex-basis: 24%">
            <p>
                <i class="fa-solid fa-money-check text-3xl"></i>
            </p>
            <h2 class="my-2 text-xl">Secure Payment</h2>
        </div>
    </div>
</div>
