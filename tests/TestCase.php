<?php

namespace Tests;

use App\Models\Permission;
use App\Models\Role;
use App\Models\User;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Illuminate\Foundation\Testing\WithFaker;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;
    use WithFaker;

    public function user_can_not_login_if_()
    {

    }

    public function user_can_register()
    {

    }

    protected function loginUser()
    {
        $user = User::factory()->create();
        return $this->actingAs($user);
    }

    protected function loginUserWithPermission($permission)
    {
        $user = User::factory()->create();
        $role = Role::factory()->create();
        $user->attachRole($role->pluck('id'));
        $permission = Permission::where('name', $permission)->first();
        $role->attachPermission($permission->pluck('id'));
        return $this->actingAs($user);
    }

    protected function loginWithSuperAdmin()
    {
        $user = User::factory()->create();
        $role_id = Role::where('name', 'super_admin')->pluck('id');
        $user->attachRole($role_id);
        return $this->actingAs($user);
    }
}
