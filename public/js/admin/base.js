$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

const DEBOUNCE_TIME = 300;

function debounce (func, wait, immediate) {
    let timeout;
    return function () {
        let context = this, args = arguments;
        let later = function () {
            timeout = null;
            if (!immediate) func.apply(context, args);
        };
        let callNow = immediate && !timeout;
        clearTimeout(timeout);
        timeout = setTimeout(later, wait);
        if (callNow) func.apply(context, args);
    };
};

const Base = (function () {
    let modules = {};

    modules.callData = function (url, data = {}, method = 'GET') {
        return $.ajax({
            url: url,
            data: data,
            method: method,
        });
    }

    modules.callWithFormData = function (url, data = {}, method = 'get') {
        return $.ajax({
            url: url,
            data: data,
            method: method,
            contentType: false,
            processData: false,
        })
    }

    modules.showError = function (arrErrors) {
        for (let value in arrErrors){
            $('.' + value + '_error').html(arrErrors[value][0]);
        }
    }

    modules.resetError = function () {
        $('.c_errors').html('');
    }

    return modules;
}(window.jQuery, window, document));

const CustomAlert = (function () {
    let modules = {};

    modules.alertSuccess = function (message) {
        Swal.fire({
            icon: 'success',
            title: message,
            showConfirmButton: false,
            timer: 1200
        })
    };

    modules.alertError = function () {
        Swal.fire({
            icon: 'error',
            title: 'Error',
        })
    }

    modules.confirmDelete = function () {
        return new Promise((resolve, reject) => {
            Swal.fire({
                title: 'Are you sure ?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.isConfirmed) {
                    resolve(true);
                } else {
                    reject(false);
                }
            })
        })
    }

    return modules;
}(window.jQuery, window, document));

$(document).ready(function () {
    $('.btn-close-modal').on('click', function () {
        $('#model-create').modal('hide');
        $('#model-update').modal('hide');
    });
});


