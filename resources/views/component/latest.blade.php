@if(count($latests) > 0)
    <h2 class="text-6xl text-center py-3">Mới nhất</h2>
    <div class="bg-white">
        <div class="max-w-2xl mx-auto py-16 px-4 sm:py-24 sm:px-6 lg:max-w-7xl lg:px-8"
             style="padding-bottom: 50px;">
            <h2 class="sr-only">Products</h2>
            <div class="grid grid-cols-1 gap-y-10 sm:grid-cols-2 gap-x-6 lg:grid-cols-3 xl:grid-cols-4 xl:gap-x-8">
                @foreach($latests as $latestProduct)
                    <a href="product/{{$latestProduct->id}}" class="group">
                        <div
                            class="w-full aspect-w-1 aspect-h-1 bg-gray-200 rounded-lg overflow-hidden xl:aspect-w-7 xl:aspect-h-8">
                            <img
                                style="height: 187px"
                                src="/uploads/products/{{$latestProduct->image}}"
                                alt="Tall slender porcelain bottle with natural clay textured body and cork stopper."
                                class="w-full h-full object-center object-cover group-hover:opacity-75">
                        </div>
                        <h3 class="mt-4 text-sm text-gray-700">{{$latestProduct->name}}</h3>
                        <p class="mt-1 text-lg font-medium text-gray-900">{{ number_format($latestProduct->price)}} đ</p>
                    </a>
                @endforeach
            </div>
            <div class="text-center mt-12">
                <a href="{{route('filterProduct')}}" class="font-bold	text-xl text-red-700">
                    Xem tất cả
                    <i class="fa-solid fa-arrow-right"></i>
                </a>
            </div>
        </div>
    </div>
@endif
