<?php

namespace App\Http\Composers;

use App\Services\PermissionService;
use Illuminate\View\View;

class PermissionComposer
{

    protected $permissionService;

    public function __construct(PermissionService $permissionService)
    {
        $this->permissionService = $permissionService;
    }


    public function compose(View $view)
    {
        $view->with('groupPermissions', $this->permissionService->getGroupPermission());
    }
}
