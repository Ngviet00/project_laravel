<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SizeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 37; $i< 45; $i++){
            DB::table('sizes')->insert([
                [
                    'size' => $i,
                ],
            ]);
        }
    }
}
