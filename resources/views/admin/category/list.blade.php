@php
    $stt = (($_GET['page'] ?? 1) - 1) * 6
@endphp
<div class="px-0 pb-2">
    <div class="table-responsive p-0">
        <table class="table align-items-center mb-0">
            <thead>
            <tr>
                <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                    STT
                </th>
                <th
                    class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
                    Name
                </th>
                <th
                    class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                    Slug
                </th>
                <th
                    class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                    Parent Name
                </th>
                <th
                    class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                    Action
                </th>
            </tr>
            </thead>
            <tbody>
            @foreach($categories as $category)
                <tr>
                    <td>
                        <p style="padding-left: 20px" class="text-sm font-weight-bold mb-0">{{ ++$stt }}</p>
                    </td>
                    <td>
                        <p class="text-sm font-weight-bold mb-0">{{ $category->name }}</p>
                    </td>
                    <td>
                        <p class="text-sm font-weight-bold mb-0 text-center">{{ $category->slug }}</p>
                    </td>
                    <td class="align-middle text-center">
                                <span class="text-secondary text-sm font-weight-bold">
                                    {!! $category->parentCategory->name ?? '----' !!}
                                </span>
                    </td>
                    <td class="align-middle d-flex justify-content-around align-items-center pt-3"
                        style="height: 42px;">
                        @hasPermission('edit_category')
                        <button type="button"
                                class='btn m-0 text-sm btn-show-modal-edit-category'
                                data-action="{{ route('categories.show', $category->id) }}">
                            <i class="fas fa-edit"></i>
                        </button>
                        @endhasPermission

                        @hasPermission('delete_category')
                        <button type="button"
                                class='btn m-0 text-sm btn-delete'
                                data-action="{{ route('categories.delete', $category->id) }}">
                            <i class="fas fa-trash-alt text-secondary"></i>
                        </button>
                        @endhasPermission
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <div class="py-3" style="margin-left: 40%;">
            @if(count($categories) == 0)
                <p style="margin-left: 22px !important;" class="m-0 fw-bold text-danger">No Data</p>
            @endif
        </div>
        <div class="d-flex justify-content-center">
            {{ $categories->appends(request()->all())->links() }}
        </div>
    </div>
</div>
