<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrderDetail extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'price',
        'quantity',
        'size',
        'total',
        'image',
        'date_order',
        'status',
        'phone',
        'user_order_id'
    ];

    public function userOrder(){
        return $this->belongsTo(UserOrder::class);
    }
}
