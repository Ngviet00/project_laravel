<?php

use App\Http\Controllers\SizeController;
use Illuminate\Support\Facades\Route;

Route::name('sizes.')->prefix('sizes')->group(function () {
    Route::get('/', [SizeController::class, 'index'])
        ->name('index');

    Route::get('/create', [SizeController::class, 'create'])
        ->name('create');

    Route::post('/', [SizeController::class, 'store'])
        ->name('store');

    Route::get('/{id}', [SizeController::class, 'show'])
        ->name('show');

    Route::get('/edit/{id}', [SizeController::class, 'edit'])
        ->name('edit');

    Route::put('/edit/{id}', [SizeController::class, 'update'])
        ->name('update');

    Route::delete('/{id}', [SizeController::class, 'destroy'])
        ->name('delete');
});
