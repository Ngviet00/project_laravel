<?php

use App\Http\Controllers\RoleController;
use Illuminate\Support\Facades\Route;

Route::name('roles.')->prefix('roles')->group(function () {
    Route::get('/', [RoleController::class, 'index'])
        ->name('index')
        ->middleware('permission:view_role');

    Route::get('/create', [RoleController::class, 'create'])
        ->name('create')
        ->middleware('permission:create_role');
    Route::post('/', [RoleController::class, 'store'])
        ->name('store')
        ->middleware('permission:store_role');

    Route::get('/{id}', [RoleController::class, 'show'])
        ->name('show')
        ->middleware('permission:show_role');

    Route::get('/edit/{id}', [RoleController::class, 'edit'])
        ->name('edit')
        ->middleware('permission:edit_role');

    Route::put('/edit/{id}', [RoleController::class, 'update'])
        ->name('update')
        ->middleware('permission:update_role');

    Route::delete('/{id}', [RoleController::class, 'destroy'])
        ->name('delete')
        ->middleware('permission:delete_role');
});
