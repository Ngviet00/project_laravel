<?php

namespace App\Http\Controllers;

use App\Http\Requests\Slide\StoreSlideRequest;
use App\Models\Slide;
use App\Services\SlideService;
use Illuminate\Http\Request;

class SlideController extends Controller
{

    protected $slideService;

    public function __construct(SlideService $slideService)
    {
        $this->slideService = $slideService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $listSlide = $this->slideService->all();
        return view('admin.slide.index', compact('listSlide'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.slide.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->slideService->create($request);
        return redirect()->route('slides.index')->with('message', 'Create Slide Success');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Slide $slide
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $slide = $this->slideService->findOrFail($id);
        return view('admin.slide.edit', compact('slide'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Slide $slide
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->slideService->update($request, $id);
        return redirect()->route('slides.index')->with('message', 'Update Slide Success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Slide $slide
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->slideService->delete($id);
        return redirect()->route('slides.index')->with('message', 'Delete Slide Success');
    }
}
