<?php

use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

Route::name('users.')->prefix('users')->group(function () {
    Route::get('/', [UserController::class, 'index'])
        ->name('index')
        ->middleware('permission:view_user');

    Route::get('/create', [UserController::class, 'create'])
        ->name('create')
        ->middleware('permission:create_user');

    Route::post('/', [UserController::class, 'store'])
        ->name('store')
        ->middleware('permission:store_user');

    Route::get('/{id}', [UserController::class, 'show'])
        ->name('show')
        ->middleware('permission:show_user');

    Route::get('/edit/{id}', [UserController::class, 'edit'])
        ->name('edit')
        ->middleware('permission:edit_user');

    Route::put('/{user}', [UserController::class, 'update'])
        ->name('update')
        ->middleware('permission:update_user');

    Route::delete('/{id}', [UserController::class, 'destroy'])
        ->name('delete')
        ->middleware(['cant_delete', 'permission:delete_user']);
});
