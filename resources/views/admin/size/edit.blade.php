@extends('admin.layouts.app')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card my-4">
                    <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
                        <div class="bg-gradient-primary shadow-primary border-radius-lg pt-4 pb-3">
                            <h6 class="text-white text-capitalize ps-3">Create Slide</h6>
                        </div>
                    </div>
                    <div class="px-0 pb-2">
                        <form method="POST" action="{{route('sizes.update', $size->id)}}" class="p-4"
                              enctype="multipart/form-data">
                            @csrf
                            @method('PUT')
                            <div class="form-group">
                                <label for="size">Size</label>
                                <input type="text" class="form-control" name="size" id="size" value="{{$size->size}}"
                                       placeholder="Size">
                            </div>
                            <a href="{{route('sizes.index')}}" class="mt-4 btn btn-dark">Back</a>
                            <button type="submit" class=" mt-4 btn btn-danger">Update</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script src="/js/admin/brand.js"></script>
@endsection
