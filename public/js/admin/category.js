const Category = (function () {
    let modules = {};

    modules.getListBy = function (url, data) {
        Base.callData(url, data)
            .done(function (res) {
                $('#list').html('').append(res);
            })
    }

    modules.getList = function () {
        let url = $('#list').data('action');
        let data = $('.form_search').serialize();
        Category.getListBy(url, data);
    }

    modules.create = function () {
        let formCreateCategory = $('#form-create-category');
        let url = formCreateCategory.data('action');
        let data = formCreateCategory.serialize();

        Base.callData(url, data, 'POST')
            .done(function (res) {
                $('#model-create').modal('hide');
                CustomAlert.alertSuccess(res.message);
                Category.resetFormCreate();
                Category.getList();
            }).fail(function (res) {
            Base.showError(res.responseJSON.errors);
        });
    }

    modules.edit = function (element) {
        let url = element.data('action');
        Base.callData(url, {}, 'GET')
            .done(function (res) {
                $('#edit-form-category').html('').append(res);
                $('#model-update').modal('show');
            }).fail(function () {
            CustomAlert.alertError();
        });
    }

    modules.update = function () {
        let formUpdateCategory = $('#update-category-form');
        let url = formUpdateCategory.data('action');
        let data = formUpdateCategory.serialize();
        Base.callData(url, data, 'PUT')
            .done(function (res) {
                $('#model-update').modal('hide');
                CustomAlert.alertSuccess(res.message);
                Category.resetFormCreate();
                Category.getList();
            }).fail(function (res) {
            Base.showError(res.responseJSON.errors);
        });
    }

    modules.delete = function (url) {
        Base.callData(url, {}, 'DELETE')
            .done(function (res) {
                CustomAlert.alertSuccess(res.message);
                Category.getList();
            }).fail(function () {
            CustomAlert.alertError();
        });
    }

    modules.resetFormCreate = function () {
        $("#form-create-category")[0].reset();
        Base.resetError();
    }

    modules.showModalCreate = function () {
        Category.resetFormCreate();
        $('#model-create').modal('show');
    }

    modules.getListByPageLink = function (element) {
        let url = element.attr('href');
        if (url !== undefined) {
            Category.getListBy(url);
        }
    }

    modules.confirmDelete = function (element) {
        let url = element.data('action');
        CustomAlert.confirmDelete().then(
            function () {
                Category.delete(url);
            }
        );
    }

    return modules;
}(window.jQuery, window, document));

$(document).ready(function () {
    Category.getList();

    $(document).on('click', '.btn-delete', function (e) {
        e.preventDefault();
        Category.confirmDelete($(this));
    });

    $(document).on('click', '.btn-show-modal-edit-category', function () {
        Category.edit($(this));
    });

    $(document).on('click', '.page-link', function (e) {
        e.preventDefault();
        Category.getListByPageLink($(this));
    });

    $('.btn-update-category').on('click', function (e) {
        e.preventDefault();
        Category.update();
    });

    $('#btn-show-modal-create').on('click', function () {
        Category.showModalCreate();
    });

    $('.btn-store-category').on('click', function (e) {
        e.preventDefault();
        Category.create();
    });

    $('#name-search').keyup(debounce(function () {
        Category.getList();
    }, DEBOUNCE_TIME));

});
