@extends('admin.layouts.app')
@section('content')
    <div class="row">
        <nav aria-label="breadcrumb" class="mb-5">
            <h3 class="font-weight-bolder mb-0">Dashboard</h3>
        </nav>
        <div class="col-xl-3 col-sm-6 mb-xl-0 mb-4">
            <div class="card">
                <div class="card-header p-3 pt-2 bg-white">
                    <div
                        class="icon icon-lg icon-shape bg-gradient-dark shadow-dark text-center border-radius-xl mt-n4 position-absolute">
                        <i class="material-icons opacity-10">lock</i>
                    </div>
                    <div class="text-end pt-1">
                        <p class="text-sm mb-0 text-capitalize">Total Role</p>
                        <h4 class="mb-0">{{$countRole}}</h4>
                    </div>
                </div>
                <div class="d-flex align-items-center justify-content-between">
                    <div class="card-footer p-3">
                        <p class="mb-0"><span class="text-success text-sm font-weight-bolder">+5% </span>than
                            month</p>
                    </div>
                    <div class="pr-3">
                        <a href="{{route('roles.index')}}" class="text-dark" style="padding-right: 10px;">View More</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-sm-6 mb-xl-0 mb-4">
            <div class="card">
                <div class="card-header p-3 pt-2 bg-white">
                    <div
                        class="icon icon-lg icon-shape bg-gradient-primary shadow-primary text-center border-radius-xl mt-n4 position-absolute">
                        <i class="material-icons opacity-10">person</i>
                    </div>
                    <div class="text-end pt-1">
                        <p class="text-sm mb-0 text-capitalize">Total User</p>
                        <h4 class="mb-0">{{$countUser}}</h4>
                    </div>
                </div>
                <div class="d-flex align-items-center justify-content-between">
                    <div class="card-footer p-3">
                        <p class="mb-0"><span class="text-success text-sm font-weight-bolder">+2% </span>than
                            hour</p>
                    </div>
                    <div class="pr-3">
                        <a href="{{route('users.index')}}" class="text-dark" style="padding-right: 10px;">View More</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-sm-6 mb-xl-0 mb-4">
            <div class="card">
                <div class="card-header p-3 pt-2 bg-white">
                    <div
                        class="icon icon-lg icon-shape bg-gradient-success shadow-success text-center border-radius-xl mt-n4 position-absolute">
                        <i class="material-icons opacity-10 ">category</i>
                    </div>
                    <div class="text-end pt-1">
                        <p class="text-sm mb-0 text-capitalize">Total Category</p>
                        <h4 class="mb-0">{{$countCategory}}</h4>
                    </div>
                </div>
                <div class="d-flex align-items-center justify-content-between">
                    <div class="card-footer p-3">
                        <p class="mb-0"><span class="text-danger text-sm font-weight-bolder">-4% </span>than
                            week</p>
                    </div>
                    <div class="pr-3">
                        <a href="{{route('categories.index')}}" class="text-dark" style="padding-right: 10px;">View
                            More</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-sm-6">
            <div class="card">
                <div class="card-header p-3 pt-2 bg-white">
                    <div
                        class="icon icon-lg icon-shape bg-gradient-info shadow-info text-center border-radius-xl mt-n4 position-absolute">
                        <i class="material-icons opacity-10">weekend</i>
                    </div>
                    <div class="text-end pt-1">
                        <p class="text-sm mb-0 text-capitalize">Total Product</p>
                        <h4 class="mb-0">{{$countProduct}}</h4>
                    </div>
                </div>
                <div class="d-flex align-items-center justify-content-between">
                    <div class="card-footer p-3">
                        <p class="mb-0"><span class="text-success text-sm font-weight-bolder">+5% </span>than
                            yesterday</p>
                    </div>
                    <div class="pr-3">
                        <a href="{{route('products.index')}}" class="text-dark" style="padding-right: 10px;">View
                            More</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-3 col-sm-6 mt-5">
            <div class="card">
                <div class="card-header p-3 pt-2 bg-white">
                    <div
                        class="icon icon-lg icon-shape bg-gradient-info shadow-info text-center border-radius-xl mt-n4 position-absolute">
                        <i class="material-icons opacity-10">weekend</i>
                    </div>
                    <div class="text-end pt-1">
                        <p class="text-sm mb-0 text-capitalize">Đơn hàng chờ xác nhận</p>
                        <h4 class="mb-0">{{$orderWait}}</h4>
                    </div>
                </div>
                <div class="d-flex align-items-center justify-content-between"
                     style="flex-direction: row-reverse;padding: 10px 0px;"
                >
                    <div class="pr-3">
                        <a href="{{route('listOrder')}}" class="text-dark" style="padding-right: 10px;">View
                            More</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-3 col-sm-6 mt-5">
            <div class="card">
                <div class="card-header p-3 pt-2 bg-white">
                    <div
                        class="icon icon-lg icon-shape bg-gradient-info shadow-info text-center border-radius-xl mt-n4 position-absolute">
                        <i class="material-icons opacity-10">weekend</i>
                    </div>
                    <div class="text-end pt-1">
                        <p class="text-sm mb-0 text-capitalize">Đơn hàng đang vận chuyển</p>
                        <h4 class="mb-0">{{$orderIsTransport}}</h4>
                    </div>
                </div>
                <div class="d-flex align-items-center justify-content-between" style="flex-direction: row-reverse;padding: 10px 0px;">
                    <div class="pr-3">
                        <a href="{{route('listOrder')}}" class="text-dark" style="padding-right: 10px;">View
                            More</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-3 col-sm-6 mt-5">
            <div class="card">
                <div class="card-header p-3 pt-2 bg-white">
                    <div
                        class="icon icon-lg icon-shape bg-gradient-info shadow-info text-center border-radius-xl mt-n4 position-absolute">
                        <i class="material-icons opacity-10">weekend</i>
                    </div>
                    <div class="text-end pt-1">
                        <p class="text-sm mb-0 text-capitalize">Đơn hàng đã giao</p>
                        <h4 class="mb-0">{{$orderSuccess}}</h4>
                    </div>
                </div>
                <div class="d-flex align-items-center justify-content-between" style="flex-direction: row-reverse;padding: 10px 0px;">
                    <div class="pr-3">
                        <a href="{{route('listOrder')}}" class="text-dark" style="padding-right: 10px;">View
                            More</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-3 col-sm-6 mt-5">
            <div class="card">
                <div class="card-header p-3 pt-2 bg-white">
                    <div
                        class="icon icon-lg icon-shape bg-gradient-info shadow-info text-center border-radius-xl mt-n4 position-absolute">
                        <i class="material-icons opacity-10">weekend</i>
                    </div>
                    <div class="text-end pt-1">
                        <p class="text-sm mb-0 text-capitalize">Đơn hàng bị hủy</p>
                        <h4 class="mb-0">{{$orderCancel}}</h4>
                    </div>
                </div>
                <div class="d-flex align-items-center justify-content-between" style="flex-direction: row-reverse;padding: 10px 0px;">
                    <div class="pr-3">
                        <a href="{{route('listOrder')}}" class="text-dark" style="padding-right: 10px;">View
                            More</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-3 col-sm-6 mt-5">
            <div class="card">
                <div class="card-header p-3 pt-2 bg-white">
                    <div
                        class="icon icon-lg icon-shape bg-gradient-info shadow-info text-center border-radius-xl mt-n4 position-absolute">
                        <i class="material-icons opacity-10">weekend</i>
                    </div>
                    <div class="text-end pt-1">
                        <p class="text-sm mb-0 text-capitalize">Tổng doanh thu</p>
                        <h4 class="mb-0">{{number_format($totalOrder)}}</h4>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
