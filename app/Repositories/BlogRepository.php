<?php

namespace App\Repositories;

use App\Models\Blog;
use JasonGuru\LaravelMakeRepository\Repository\BaseRepository;
//use Your Model

/**
 * Class BlogRepository.
 */
class BlogRepository extends BaseRepository
{
    /**
     * @return string
     *  Return the model
     */
    public function model()
    {
        return Blog::class;
    }
}
