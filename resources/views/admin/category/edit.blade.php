<form id="update-category-form" data-action="{{route('categories.update', $category->id)}}" class="p-4 pl-0 pt-1" style="padding: 0px !important;">
    <div class="col-xs-12 col-sm-12 col-md-12 mt-3">
        <div class="form-group custom-form-group-create">
            <strong class="form-check" style="padding: 0px">Name: </strong>
            <div class="form-check input-group input-group-dynamic info-horizontal" style="padding: 0px">
                <input type="text" name="name" class="form-control shadow-none" style="outline: none"
                       value="{{$category->name}}" onfocus="focused(this)" onfocusout="defocused(this)">
            </div>
            <input type="hidden" name="slug" id="slug_create" value="{{$category->slug}}"
                   class="form-control shadow-none" style="outline: none"
                   onfocus="focused(this)" onfocusout="defocused(this)">
            <div class="text-danger name_error"></div>
        </div>
    </div>
    <div class="form-group">
        <label for="" class="text-lg text-bold m-0 my-2 text-dark">Parent Category</label>
        <select class="form-select form-select-sm shadow-none p-0" name="parent_id"
                style="border-top: none;border-right: none;border-left: none;">
            <option value="">None</option>
            @foreach($parentCategory as $cate)
                @if($category->id != $cate->id)
                    <option
                        value="{{$cate->id}}" {{ ( $cate->id == $category->parent_id ) ? 'selected' : '' }} >{{$cate->name}}</option>
                @endif
            @endforeach
        </select>
    </div>
</form>
