<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            [
                'name' => 'Quần Áo Nam',
                'slug' => 'quan-ao-nam',
                'parent_id' => null
            ],
            [
                'name' => 'Quần Áo Nữ',
                'slug' => 'quan-ao-nu',
                'parent_id' => null
            ],
            [
                'name' => 'Áo cộc nam',
                'slug' => 'ao-coc-nam',
                'parent_id' => 1
            ],
            [
                'name' => 'Chân váy nữ',
                'slug' => 'chan-vay-nu',
                'parent_id' => 2
            ]
        ]);
    }
}
