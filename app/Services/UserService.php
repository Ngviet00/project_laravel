<?php

namespace App\Services;

use App\Repositories\UserRepository;

class UserService
{
    protected $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function create($request)
    {
        $dataCreate = $request->all();

        $role = $request->role ?? [];

        $user = $this->userRepository->create($dataCreate);

        $user->attachRole($role);

        return $user;
    }

    public function search($request)
    {
        $dataSearch = $request->all();

        $dataSearch['name'] = $request->name ?? '';

        $dataSearch['role_id'] = $request->role_id ?? '';

        return $this->userRepository->search($dataSearch);
    }

    public function find($id)
    {
        return $this->userRepository->findOrFail($id);
    }

    public function delete($id)
    {
        $user = $this->userRepository->findOrFail($id);

        $user->detachRole();

        $user->delete();

        return $user;
    }

    public function update($request, $id)
    {
        $user = $this->userRepository->findOrFail($id);

        $role = $request->role ?? [];

        $dataUpdate = $request->all();

        $user->syncRole($role);

        $user->update($dataUpdate);

        return $user;
    }
}
