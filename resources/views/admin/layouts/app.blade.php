<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<script src="//cdn.ckeditor.com/4.16.2/standard/ckeditor.js"></script>
@include('admin.layouts.head')
<body class="g-sidenav-show  bg-gray-200">
@include('admin.layouts.sidebar')
<main class="main-content position-relative max-height-vh-100 h-100 border-radius-lg ">
    @include('admin.layouts.navbar')
    <div class="container-fluid">
        <div>
            @yield('content')
        </div>
    </div>
</main>
<script src="/assets/js/material-dashboard.min.js?v=3.0.1"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.4.7/dist/sweetalert2.all.min.js"></script>
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script src="/js/admin/base.js"></script>
<script src="{{ asset('js/app.js') }}" defer></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js"></script>
@yield('js')
</body>
