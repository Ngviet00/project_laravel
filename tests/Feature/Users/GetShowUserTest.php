<?php

namespace Tests\Feature\Users;

use App\Models\User;
use Illuminate\Http\Response;
use Tests\TestCase;

class GetShowUserTest extends TestCase
{
    /** @test */
    public function unauthenticated_can_not_get_one_user()
    {
        $userCreated = $this->createFactoryUser();
        $response = $this->get($this->getOneUserRoute($userCreated['id']));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect(route('login'));
    }

    /** @test */
    public function authenticated_with_permission_can_get_one_user()
    {
        $this->loginUserWithPermission('show_user');
        $userCreated = $this->createFactoryUser();
        $response = $this->get($this->getOneUserRoute($userCreated['id']));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('admin.user.show');
        $response->assertSee($userCreated['name']);
    }

    /** @test */
    public function authenticated_without_permission_can_not_get_one_user()
    {
        $this->loginUser();
        $userCreated = $this->createFactoryUser();
        $response = $this->get($this->getOneUserRoute($userCreated['id']));
        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    /** @test */
    public function authenticated_with_permission_can_not_get_one_user_if_not_exists()
    {
        $this->loginUserWithPermission('show_user');
        $userId = -1;
        $response = $this->get($this->getOneUserRoute($userId));
        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }

    protected function getOneUserRoute($id): string
    {
        return route('users.show', $id);
    }

    public function createFactoryUser(): array
    {
        return User::factory()->create()->toArray();
    }
}
