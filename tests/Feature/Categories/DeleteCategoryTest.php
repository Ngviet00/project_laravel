<?php

namespace Tests\Feature\Categories;

use App\Models\Category;
use Illuminate\Http\Response;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class DeleteCategoryTest extends TestCase
{
    /** @test */
    public function authenticated_with_permission_can_delete_category()
    {
        $this->loginUserWithPermission('delete_category');
        $categoryCreated = $this->createFactoryCategory();
        $response = $this->deleteJson($this->getDeleteCategoryRoute($categoryCreated['id']));
        $response->assertStatus(Response::HTTP_OK);
        $response
            ->assertJson(fn(AssertableJson $json) => $json->where('status_code', Response::HTTP_OK)
                ->where('message', 'Delete Category Success')
                ->etc()
            );
        $this->assertDatabaseMissing('categories', $categoryCreated);
    }

    /** @test */
    public function authenticated_super_admin_can_delete_category()
    {
        $this->loginWithSuperAdmin();
        $categoryCreated = $this->createFactoryCategory();
        $response = $this->deleteJson($this->getDeleteCategoryRoute($categoryCreated['id']));
        $response->assertStatus(Response::HTTP_OK);
        $response
            ->assertJson(fn(AssertableJson $json) => $json->where('status_code', Response::HTTP_OK)
                ->where('message', 'Delete Category Success')
                ->etc()
            );
        $this->assertDatabaseMissing('categories', $categoryCreated);
    }

    /** @test */
    public function authenticated_without_permission_can_not_delete_category()
    {
        $this->loginUser();
        $categoryCreated = $this->createFactoryCategory();
        $response = $this->delete($this->getDeleteCategoryRoute($categoryCreated['id']));
        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    /** @test */
    public function unauthenticated_can_not_delete_category()
    {
        $categoryCreated = $this->createFactoryCategory();
        $response = $this->delete($this->getDeleteCategoryRoute($categoryCreated['id']));
        $response->assertRedirect(route('login'));
    }

    public function getDeleteCategoryRoute($id): string
    {
        return route('categories.delete', $id);
    }

    public function createFactoryCategory(): array
    {
        return Category::factory()->create()->toArray();
    }
}
