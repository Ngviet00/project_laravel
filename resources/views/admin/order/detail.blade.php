@extends('admin.layouts.app')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card my-4">
                    <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
                        <div class="bg-gradient-primary shadow-primary border-radius-lg pt-4 pb-3">
                            <h6 class="text-white text-capitalize ps-3">Show Order detail</h6>
                        </div>
                    </div>

                    <div class="p-4">
                        <label for="name" class="text-lg text-bold d-block text-dark">ID: {{ $detailOrder->id }}</label>
                        <label for="name" class="text-lg text-bold d-block text-dark">Tên khách hàng: {{ $detailOrder->userOrder->name }}</label>
                        <label for="name" class="text-lg text-bold d-block text-dark">Email: {{ $detailOrder->userOrder->email }}</label>
                        <label for="name" class="text-lg text-bold d-block text-dark">Số điện thoại: {{ $detailOrder->userOrder->phone }}</label>
                        <label for="name" class="text-lg text-bold d-block text-dark">Địa chỉ: {{ $detailOrder->userOrder->address }}</label>

                        <hr>
                        <label for="name" class="text-lg text-bold d-block text-dark">Tên sản
                            phẩm: {{ $detailOrder->name }}</label>
                        <label for="name" class="text-lg text-bold d-block text-dark">Hình ảnh:
                            <img src="{{$detailOrder->image}}" alt="" width="100">
                        </label>
                        <label for="name" class="text-lg text-bold d-block text-dark">Số
                            lượng: {{ $detailOrder->quantity }}</label>
                        <label for="name"
                               class="text-lg text-bold d-block text-dark">Size: {{ $detailOrder->size }}</label>
                        <label for="name"
                               class="text-lg text-bold d-block text-dark">Giá: {{ $detailOrder->price }}</label>
                        <label for="name" class="text-lg text-bold d-block text-dark">Thành
                            tiền: {{ $detailOrder->total }}</label>
                        <label for="name" class="text-lg text-bold d-block text-dark">Ngày
                            đặt: {{ $detailOrder->date_order }}</label>
                        <label for="name" class="text-lg text-bold d-block text-dark">
                            Trạng thái:
                            @if($detailOrder->status == 1)
                                <span style="color: black;font-weight: bold">Chờ xác nhận</span>
                            @elseif($detailOrder->status == 2)
                                <span class="text-primary fw-bold">Đang giao hàng</span>
                            @elseif($detailOrder->status == 3)
                                <span class="text-success fw-bold">Đã giao</span>
                            @else
                                <span class="text-danger fw-bold">Đã hủy</span>
                            @endif
                        </label>
                        <label for="name" class="text-lg text-bold d-block text-dark">
                        </label>
                        <a href="{{ route('listOrder') }}" class="btn btn-dark">Back</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
