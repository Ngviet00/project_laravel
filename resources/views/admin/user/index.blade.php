@extends('admin.layouts.app')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card my-4">
                    <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
                        <div class="bg-gradient-primary shadow-primary border-radius-lg pt-4 pb-3">
                            <h6 class="text-white text-capitalize ps-3">Product</h6>
                        </div>
                    </div>

                    <div class="pt-3 d-flex justify-content-between">
                        <div>
                            @if(Session::has('message'))
                                <p class="fw-bold text-success pt-2"
                                   style="padding-left:17px">{!! Session::get('message') !!}
                                </p>
                            @endif
                        </div>
                        <div>
                            <div class="input-group">
                                <form action="" method="GET" class="d-flex align-items-center">
                                    <div class="form-outline d-flex">
                                        <select name="role_id" id="role_id" style="margin-right: 10px;
                                            outline: none;
                                            border-color: #d6d6d6;
                                            background: transparent;
                                            border-radius: 4px;">
                                            <option value="" selected>Role</option>
                                            @foreach($roles as $role)
                                                <option value="{{$role->id}}">{{$role->name}}</option>
                                            @endforeach
                                        </select>
                                        <input type="search" id="name" name="name" value="{{ request('name') }}"
                                               class="form-control pl-2 shadow-none"
                                               placeholder="Search"/>
                                    </div>
                                    <button type="submit" class="btn btn-primary m-0">
                                        <i class="fas fa-search"></i>
                                    </button>
                                </form>
                            </div>
                        </div>
                        <a href="{{route('users.create')}}" class="btn btn-danger d-inline-block text-capitalize"
                           style="margin-right:17px">Create</a>
                    </div>

                    <div class="px-0 pb-2">
                        <div class="table-responsive p-0">
                            <table class="table align-items-center mb-0">
                                <thead>
                                <tr>
                                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                        ID
                                    </th>
                                    <th
                                        class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
                                        Name
                                    </th>
                                    <th
                                        class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                        Email
                                    </th>
                                    <th
                                        class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                        Gender
                                    </th>
                                    <th
                                        class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                        Role
                                    </th>
                                    <th
                                        class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                        Action
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($users as $user)
                                    <tr>
                                        <td>
                                            <p style="padding-left: 20px" class="text-sm font-weight-bold mb-0">
                                                {{ $user->id }}</p>
                                        </td>
                                        <td>
                                            <p class="text-sm font-weight-bold mb-0">{{ $user->name }}</p>
                                        </td>
                                        <td>
                                            <p class="text-sm font-weight-bold mb-0 text-center">
                                                {{$user->email}}
                                            </p>
                                        </td>
                                        <td>
                                            <p class="text-sm font-weight-bold mb-0 text-center">
                                                {{ $user->gender == 1 ? 'Nam' : 'Nu' }}
                                            </p>
                                        </td>
                                        <td class="align-middle text-center text-sm">
                                            <div class="d-flex flex-wrap">
                                                @foreach($user->roles as $role)
                                                    <span style="font-size: 14px;">
                                                        {{ $role->name }},&nbsp&nbsp
                                                    </span>
                                                @endforeach
                                            </div>
                                        </td>
                                        <td class="align-middle d-flex justify-content-around align-items-center pt-3">
                                            @hasPermission('show_user')
                                            <a href="{{ route('users.show', $user->id) }}"
                                               class="text-secondary mr-2 d-inline-block font-weight-bold text-sm text-decoration-none">
                                                <i class="fas fa-eye"></i>
                                            </a>
                                            @endhasPermission

                                            @hasPermission('edit_user')
                                            <a href="{{ route('users.edit', $user->id) }}"
                                               class="text-secondary mr-2 font-weight-bold text-sm d-inline-block text-decoration-none">
                                                <i class="fas fa-edit"></i>
                                            </a>
                                            @endhasPermission

                                            @hasPermission('delete_user')
                                            <form action="{{route('users.delete', $user->id)}}" method="POST">
                                                @csrf
                                                @method('DELETE')
                                                <button class='btn m-0 text-sm'
                                                        onclick="return confirm('Are you sure?')">
                                                    <i class="fas fa-trash-alt text-secondary"></i>
                                                </button>
                                            </form>
                                            @endhasPermission
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            @if(count($users) == 0)
                                <div class="py-3" style="margin-left: 40%;">
                                    <p style="margin-left: 22px !important;" class="m-0 fw-bold text-danger">No Data</p>
                                </div>
                            @endif
                            <div class="d-flex justify-content-center">
                                {{ $users->appends(request()->all())->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
