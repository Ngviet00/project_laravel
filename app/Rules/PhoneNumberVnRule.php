<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class PhoneNumberVnRule implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $regex = "/((^(\+84|84|0){1})(3|5|7|8|9))+([0-9]{8})$/";
        return preg_match($regex, $value);
    }

    public function message()
    {
        return 'The :attribute format is invalid.';
    }
}
