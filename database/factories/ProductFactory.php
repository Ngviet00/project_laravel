<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Http\UploadedFile;

class ProductFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        return [
            'name' => $this->faker->name,
            'price' => $this->faker->numberBetween($min = 100, $max = 1000),
            'desc' => $this->faker->text,
            'image' => UploadedFile::fake()->image('image.jpg'),
        ];
    }
}
