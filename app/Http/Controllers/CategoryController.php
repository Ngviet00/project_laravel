<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateCategoryRequest;
use App\Http\Requests\UpdateCategoryRequest;
use App\Http\Resources\CategoryResource;
use App\Services\CategoryService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class CategoryController extends Controller
{

    protected $categoryService;

    public function __construct(CategoryService $categoryService)
    {
        $this->categoryService = $categoryService;
    }

    public function index()
    {
        return view('admin.category.index');
    }

    public function list(Request $request)
    {
        $categories = $this->categoryService->search($request);
        return view('admin.category.list', compact('categories'));
    }

    public function show($id)
    {
        $category = $this->categoryService->findOrFail($id);
        return view('admin.category.edit', compact('category'));
    }

    public function store(CreateCategoryRequest $request)
    {
        $category = $this->categoryService->store($request);
        $categoryResource = new CategoryResource($category);
        return $this->successResponse(
            $categoryResource,
            'Create Category Success',
            Response::HTTP_OK
        );
    }

    public function update(UpdateCategoryRequest $request, $id)
    {
        $category = $this->categoryService->update($request, $id);
        $categoryResource = new CategoryResource($category);
        return $this->successResponse(
            $categoryResource,
            'Update Category Success',
            Response::HTTP_OK
        );
    }

    public function destroy($id)
    {
        $this->categoryService->delete($id);
        return $this->successResponse(
            '',
            'Delete Category Success',
            Response::HTTP_OK
        );
    }
}
