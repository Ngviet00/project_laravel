<?php

namespace Tests\Feature\Products;

use App\Models\Product;
use Illuminate\Http\Response;
use Illuminate\Http\UploadedFile;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class UpdateProductTest extends TestCase
{
    /** @test */
    public function unauthenticated_can_not_view_form_edit_product()
    {
        $productExists = $this->createFactoryProduct();
        $response = $this->get($this->getEditProductRoute($productExists['id']));
        $response->assertRedirect(route('login'));
    }

    /** @test */
    public function authenticated_without_permission_can_not_view_form_edit_product()
    {
        $this->loginUser();
        $productExists = $this->createFactoryProduct();
        $response = $this->get($this->getEditProductRoute($productExists['id']));
        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    /** @test */
    public function authenticated_with_permission_can_view_form_edit_product()
    {
        $this->loginUserWithPermission('edit_product');
        $productExists = $this->createFactoryProduct();
        $response = $this->get($this->getEditProductRoute($productExists['id']));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('admin.product.edit');
    }

    /** @test */
    public function authenticated_without_permission_can_not_update_product()
    {
        $this->loginUser();
        $existsProduct = $this->createFactoryProduct();
        $dataUpdate = $this->makeFactoryProduct();
        $response = $this->post($this->updateProductRoute($existsProduct['id']), $dataUpdate);
        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    /** @test */
    public function authenticated_with_permission_can_not_update_product_if_name_is_null()
    {
        $this->loginUserWithPermission('update_product');
        $existsProduct = $this->createFactoryProduct();
        $dataUpdate = $this->makeFactoryProductSetFieldNull('name');
        $response = $this->postJson($this->updateProductRoute($existsProduct['id']), $dataUpdate);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertJsonValidationErrors(['name']);
    }

    /** @test */
    public function authenticated_with_permission_can_not_update_product_if_price_is_null()
    {
        $this->loginUserWithPermission('update_product');
        $existsProduct = $this->createFactoryProduct();
        $dataUpdate = $this->makeFactoryProductSetFieldNull('price');
        $response = $this->postJson($this->updateProductRoute($existsProduct['id']), $dataUpdate);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertJsonValidationErrors(['price']);
    }

    /** @test */
    public function authenticated_with_permission_can_not_update_product_if_price_is_number()
    {
        $this->loginUserWithPermission('update_product');
        $existsProduct = $this->createFactoryProduct();
        $dataUpdate = Product::factory()->make(['price' => 'abc2321'])->toArray();
        $response = $this->postJson($this->updateProductRoute($existsProduct['id']), $dataUpdate);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertJsonValidationErrors(['price']);
    }

    /** @test */
    public function authenticated_with_permission_can_not_update_product_if_desc_is_null()
    {
        $this->loginUserWithPermission('update_product');
        $existsProduct = $this->createFactoryProduct();
        $dataUpdate = $this->makeFactoryProductSetFieldNull('desc');
        $response = $this->postJson($this->updateProductRoute($existsProduct['id']), $dataUpdate);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertJsonValidationErrors(['desc']);
    }

    /** @test */
    public function authenticated_with_permission_can_not_update_product_if_image_is_invalid()
    {
        $this->loginUserWithPermission('update_product');
        $existsProduct = $this->createFactoryProduct();
        $image = UploadedFile::fake()->create('image.pdf');
        $dataUpdate = Product::factory()->make(['image' => $image])->toArray();
        $response = $this->postJson($this->updateProductRoute($existsProduct['id']), $dataUpdate);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertJsonValidationErrors(['image']);
    }

    /** @test */
    public function authenticated_with_permission_can_update_product_if_image_is_null()
    {
        $this->loginUserWithPermission('update_product');
        $existsProduct = $this->createFactoryProduct();
        $dataUpdate = $this->makeFactoryProductSetFieldNull('image');
        $response = $this->postJson(route('products.update', $existsProduct['id']), $dataUpdate);
        $response->assertStatus(Response::HTTP_OK);
        $response->assertJson(fn(AssertableJson $json) => $json->where('status_code', Response::HTTP_OK)
            ->where('message', 'Update Product Success')
            ->etc()
        );
        $this->assertDatabaseHas('products', [
            'name' => $dataUpdate['name']
        ]);
    }

    /** @test */
    public function authenticated_with_permission_can_update_product_if_data_is_valid()
    {
        $this->loginUserWithPermission('update_product');
        $existsProduct = $this->createFactoryProduct();
        $image = UploadedFile::fake()->image('img_update.jpg');
        $dataUpdate = Product::factory()->make(['image' => $image])->toArray();
        $response = $this->postJson(route('products.update', $existsProduct['id']), $dataUpdate);
        $response->assertStatus(Response::HTTP_OK);
        $response->assertJson(fn(AssertableJson $json) => $json->where('status_code', Response::HTTP_OK)
            ->where('message', 'Update Product Success')->etc()
        );
        $this->assertDatabaseHas('products', [
            'name' => $dataUpdate['name']
        ]);
    }

    public function getEditProductRoute($id): string
    {
        return route('products.show', $id);
    }

    public function updateProductRoute($id): string
    {
        return route('products.update', $id);
    }

    public function makeFactoryProduct(): array
    {
        return Product::factory()->make()->toArray();
    }

    public function createFactoryProduct(): array
    {
        return Product::factory()->create()->toArray();
    }

    public function makeFactoryProductSetFieldNull($field, $value = null): array
    {
        return Product::factory()->make([
            $field => $value
        ])->toArray();
    }
}
