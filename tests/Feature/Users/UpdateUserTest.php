<?php

namespace Tests\Feature\Users;

use App\Models\User;
use Illuminate\Http\Response;
use Tests\TestCase;

class UpdateUserTest extends TestCase
{
    /** @test */
    public function unauthenticated_can_not_view_form_edit_user()
    {
        $userCreated = $this->getDataCreate();
        $response = $this->get($this->getEditUserRoute($userCreated['id']));
        $response->assertRedirect(route('login'));
    }

    /** @test */
    public function authenticated_without_permission_can_not_view_form_edit_user()
    {
        $this->loginUser();
        $userCreated = $this->getDataCreate();
        $response = $this->get($this->getEditUserRoute($userCreated['id']));
        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    /** @test */
    public function authenticated_with_permission_can_view_form_edit_user()
    {
        $this->loginUserWithPermission('edit_user');
        $userCreated = $this->getDataCreate();
        $response = $this->get($this->getEditUserRoute($userCreated['id']));
        $response->assertViewIs('admin.user.edit');
    }

    /** @test */
    public function authenticated_without_permission_can_not_update_user()
    {
        $this->loginUser();
        $existsUser = $this->getDataCreate();
        $dataUpdate = $this->makeFactoryUser();
        $response = $this->put($this->updateUserRoute($existsUser['id']), $dataUpdate);
        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    /** @test */
    public function authenticated_with_permission_can_not_update_user_if_name_is_null()
    {
        $this->loginUserWithPermission('update_user');
        $existsUser = $this->getDataCreate();
        $dataUpdate = $this->makeFactoryUserSetFieldNull('name');
        $response = $this->put($this->updateUserRoute($existsUser['id']), $dataUpdate);
        $response->assertSessionHasErrors(['name']);
    }

    /** @test */
    public function authenticated_with_permission_can_not_update_user_if_email_is_null()
    {
        $this->loginUserWithPermission('update_user');
        $existsUser = $this->getDataCreate();
        $dataUpdate = $this->makeFactoryUserSetFieldNull('email');
        $response = $this->put($this->updateUserRoute($existsUser['id']), $dataUpdate);
        $response->assertSessionHasErrors(['email']);
    }

    /** @test */
    public function authenticated_with_permission_can_not_update_user_if_phone_is_null()
    {
        $this->loginUserWithPermission('update_user');
        $existsUser = $this->getDataCreate();
        $dataUpdate = $this->makeFactoryUserSetFieldNull('phone');
        $response = $this->put($this->updateUserRoute($existsUser['id']), $dataUpdate);
        $response->assertSessionHasErrors(['phone']);
    }

    /** @test */
    public function authenticated_with_permission_can_not_update_user_if_phone_is_invalid()
    {
        $this->loginUserWithPermission('update_user');
        $existsUser = $this->getDataCreate();
        $dataUpdate = User::factory()->make([
            'phone' => '122335477'
        ])->toArray();
        $response = $this->put($this->updateUserRoute($existsUser['id']), $dataUpdate);
        $response->assertSessionHasErrors(['phone']);
    }

    /** @test */
    public function authenticated_with_permission_can_update_user_if_data_is_valid()
    {
        $this->loginUserWithPermission('update_user');
        $existsUser = $this->getDataCreate();
        $dataUpdate = $this->makeFactoryUser();
        $dataUpdate['password'] = $this->faker->password(8);
        $response = $this->put($this->updateUserRoute($existsUser['id']), $dataUpdate);
        $response->assertStatus(Response::HTTP_FOUND);
        $this->assertDatabaseHas('users', [
            'email' => $dataUpdate['email'],
            'name' => $dataUpdate['name']
        ]);
        $response->assertRedirect(route('users.index'));
    }

    public function getEditUserRoute($id): string
    {
        return route('users.edit', $id);
    }

    public function updateUserRoute($data): string
    {
        return route('users.update', $data);
    }

    public function getDataCreate(): array
    {
        return User::factory()->create()->toArray();
    }

    public function makeFactoryUser(): array
    {
        return User::factory()->make()->toArray();
    }

    public function makeFactoryUserSetFieldNull($field): array
    {
        return User::factory()->make([
            $field => null
        ])->toArray();
    }
}
