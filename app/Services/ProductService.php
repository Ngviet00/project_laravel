<?php

namespace App\Services;

use App\Repositories\ProductRepository;
use App\Traits\HandleImage;

class ProductService
{
    use HandleImage;

    protected $productRepository;

    public function __construct(ProductRepository $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    public function all()
    {
        return $this->productRepository->all();
    }

    public function search($request)
    {
        $dataSearch = $request->all();

        $dataSearch['name'] = $request->name ?? '';

        $dataSearch['category_id'] = $request->categoryId ?? '';

        $dataSearch['min_price'] = $request->minPrice ?? '';

        $dataSearch['max_price'] = $request->maxPrice ?? '';

        return $this->productRepository->search($dataSearch);
    }

    public function create($request)
    {
        $data = $request->all();

        $data['image'] = $this->saveImage($request);

        $product = $this->productRepository->create($data);

        $product->sizes()->attach($data['size_id']);

        return $product;
    }

    public function findOrFail($id)
    {
        return $this->productRepository->findOrFail($id);
    }

    public function update($request, $id)
    {
        $dataUpdate = $request->all();

        $product = $this->productRepository->findOrFail($id);

        $dataUpdate['image'] = $this->updateImage($request, $product->image);

        $product->update($dataUpdate);

        return $product;
    }

    public function delete($id)
    {
        $product = $this->productRepository->findOrFail($id);

        $this->deleteImage($product->image);

        $product->delete();

        return $product;
    }
}
