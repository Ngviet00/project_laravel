<?php

namespace App\Http\Controllers;

use App\Http\Requests\Product\CreateProductRequest;
use App\Http\Requests\Product\UpdateProductRequest;
use App\Http\Resources\ProductResource;
use App\Models\Brand;
use App\Models\Category;
use App\Models\Size;
use App\Services\ProductService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ProductController extends Controller
{
    protected $productService;

    public function __construct(ProductService $productService)
    {
        $this->productService = $productService;
    }

    public function index()
    {
        $sizes = Size::all();
        $brands = Brand::all();
        return view('admin.product.index', compact('sizes', 'brands'));
    }


    public function create()
    {
        $brands = Brand::all();
        $categories = Category::all();
        $sizes = Size::all();
        return view('admin.product.create', compact('brands', 'sizes', 'categories'));
    }

    public function list(Request $request)
    {
        $products = $this->productService->search($request);
        return view('admin.product.list', compact('products'));
    }

    public function store(CreateProductRequest $request)
    {
        $this->productService->create($request);
        return redirect(route('products.index'));
    }

    public function show($id)
    {
        $product = $this->productService->findOrFail($id);
        $brands = Brand::all();
        $sizes = Size::all();
        $categories = Category::all();
        return view('admin.product.edit', compact('product', 'brands', 'sizes'));
    }

    public function update(Request $request, $id)
    {
        $this->productService->update($request, $id);
        return redirect(route('products.index'));
    }

    public function destroy($id)
    {
        $this->productService->delete($id);
        return $this->successResponse(
            '',
            'Delete Product Success',
            Response::HTTP_OK
        );
    }
}
