@extends('admin.layouts.app')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card my-4">
                    <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
                        <div class="bg-gradient-primary shadow-primary border-radius-lg pt-4 pb-3">
                            <h6 class="text-white text-capitalize ps-3">Show User</h6>
                        </div>
                    </div>

                    <div class="p-4">
                        <label for="name" class="text-lg text-bold d-block text-dark">ID: {{ $user->id }}</label>
                        <label for="name" class="text-lg text-bold d-block text-dark">Name: {{ $user->name }}</label>
                        <label for="name" class="text-lg text-bold d-block text-dark">Price: {{ $user->email }}</label>
                        <label for="name"
                               class="text-lg text-bold d-block text-dark">Address: {{ $user->address }}</label>
                        <label for="name" class="text-lg text-bold d-block text-dark">Date of
                            Birth: {{ $user->dob }}</label>
                        <label for="name" class="text-lg text-bold d-block text-dark">Phone: {{ $user->phone }}</label>
                        <label for="name"
                               class="text-lg text-bold d-block text-dark">Gender: {{ $user->gender == '1' ? 'Nam' : 'Nu' }}</label>
                        <label for="name" class="text-lg text-bold d-block text-dark">
                            Role:
                            @foreach($user->roles as $role)
                                <td class="align-middle text-center text-sm">
                                    <span class="badge badge-sm bg-gradient-success">{{ $role->name }}</span>
                                </td>
                            @endforeach
                        </label>
                        <a href="{{ route('users.index') }}" class="btn btn-dark">Back</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
