@extends('layouts.app_client')
@section('content')
    <section class="h-screen">
        <div class="px-6 h-full text-gray-800">
            <div class="mt-6 xl:ml-20 xl:w-12/12 lg:w-12/12 md:w-12/12 mb-12 md:mb-0">
                @if($userOrder)
                    <div class="d-flex justify-content-between align-items-center">
                        <h2 style="font-weight: bold" class="text-2xl my-4">Xin chào {{$userOrder->name}}
                            - {{$userOrder->phone}} - Lịch sử mua hàng</h2>
                        <a href="{{route('logoutPhone')}}" class="text-danger">Thoát tài khoản</a>
                    </div>

                    <table class="table">
                        <thead>
                        <tr>
                            <th scope="col">Mã đơn hàng</th>
                            <th scope="col">Sản phẩm</th>
                            <th scope="col">Số lượng</th>
                            <th scope="col">Size</th>
                            <th scope="col">Giá</th>
                            <th scope="col">Thành tiền</th>
                            <th scope="col">Ngày đặt mua</th>
                            <th scope="col">Trạng thái</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($listOrderHistory as $item)
                            <tr style="vertical-align: middle">
                                <th scope="row">{{$item->id}}</th>
                                <td class="d-flex align-items-center">
                                    <img src="{{$item->image}}" alt="" style="width: 80px ; padding-right: 10px;">
                                    {{$item->name}}
                                </td>
                                <td>{{number_format($item->quantity)}}</td>
                                <td>{{number_format($item->size)}}</td>
                                <td>{{number_format($item->price)}}</td>
                                <td>{{number_format($item->price * $item->quantity)}}</td>
                                <td>{{$item->date_order}}</td>
                                <td>
                                    @if($item->status == 1)
                                        <span style="color: black;font-weight: bold">Chờ xác nhận</span>
                                    @elseif($item->status == 2)
                                        <span class="text-primary fw-bold">Đang giao hàng</span>
                                    @elseif($item->status == 3)
                                        <span class="text-success fw-bold">Đã giao</span>
                                    @else
                                        <span class="text-danger fw-bold">Đã hủy</span>
                                    @endif
                                </td>
                            </tr>
                        @empty
                            <p>Not found data</p>
                        @endforelse
                        </tbody>
                    </table>
                @else
                    <h2 class="text-2xl">Not found data</h2>
                @endif
            </div>
        </div>
    </section>
@endsection
