<?php

namespace App\Providers;

use App\Http\Composers\CategoryComposer;
use App\Http\Composers\PermissionComposer;
use App\Http\Composers\ProductComposer;
use App\Http\Composers\RoleComposer;
use App\Http\Composers\UserComposer;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ViewServiceProvider extends ServiceProvider
{
    public function register()
    {

    }

    public function boot()
    {
        View::composer([
            'admin.dashboard',
            'admin.category.index',
            'admin.category.edit',
            'admin.product.create',
            'admin.product.edit',
            'admin.product.index'
        ], CategoryComposer::class);

        View::composer([
            'admin.dashboard',
            'admin.user.create',
            'admin.user.edit',
            'admin.user.index',
        ], RoleComposer::class);

        View::composer([
            'admin.role.create',
            'admin.role.edit'
        ], PermissionComposer::class);

        View::composer([
            'admin.dashboard',
        ], UserComposer::class);

        View::composer([
            'admin.dashboard',
        ], ProductComposer::class);
    }
}
