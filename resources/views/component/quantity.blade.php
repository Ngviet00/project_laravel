<div class="custom-number-input h-10 w-32 mt-4">
    <label for="custom-input-number" class="w-full text-gray-700 text-sm font-semibold">
        Số lượng:
    </label>
    <div class="flex flex-row h-10 w-full rounded-lg relative bg-transparent mt-1">
        <button onclick="decrement()"
                type="button"
                class="border bg-white text-gray-600 hover:text-gray-700 hover:bg-gray-400 h-full w-20 rounded-l cursor-pointer outline-none">
            <span class="m-auto text-2xl font-thin">−</span>
        </button>
        <input type="number"
               id="quantity_product"
               class="border get_quantity outline-none focus:outline-none text-center w-full bg-white font-semibold text-md hover:text-black focus:text-black  md:text-basecursor-default flex items-center text-gray-700  outline-none"
               name="custom-input-number" value="1" min="1">
        <button
                type="button"
                onclick="increment()"
                class="border bg-white text-gray-600 hover:text-gray-700 hover:bg-gray-400 h-full w-20 rounded-r cursor-pointer">
            <span class="m-auto text-2xl font-thin">+</span>
        </button>
    </div>
</div>
<style>
    input[type='number']::-webkit-inner-spin-button,
    input[type='number']::-webkit-outer-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }

    .custom-number-input input:focus {
        outline: none !important;
    }

    .custom-number-input button:focus {
        outline: none !important;
    }
</style>
<script>
    function decrement() {
        let quantity = Number($('.get_quantity').val());
        console.log(quantity)
        if (quantity > 1)
        {
            quantity--;
            $('.get_quantity').val(quantity);
        }
    }

    function increment() {
        let quantity = Number($('.get_quantity').val());
        quantity++;
        $('.get_quantity').val(quantity);
    }
</script>
