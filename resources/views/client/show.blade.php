@extends('layouts.app_client')
@section('content')
    <nav class="flex py-6 bg-white" aria-label="Breadcrumb" style="padding-left: 8rem;">
        <ol class="inline-flex items-center space-x-1 md:space-x-3">
            <li class="inline-flex items-center">
                <a href="/"
                   class="inline-flex items-center text-sm text-gray-700 hover:text-gray-900 dark:text-gray-400 dark:hover:text-white">
                    <svg class="mr-2 w-4 h-4" fill="currentColor" viewBox="0 0 20 20"
                         xmlns="http://www.w3.org/2000/svg">
                        <path
                            d="M10.707 2.293a1 1 0 00-1.414 0l-7 7a1 1 0 001.414 1.414L4 10.414V17a1 1 0 001 1h2a1 1 0 001-1v-2a1 1 0 011-1h2a1 1 0 011 1v2a1 1 0 001 1h2a1 1 0 001-1v-6.586l.293.293a1 1 0 001.414-1.414l-7-7z"></path>
                    </svg>
                    Trang chủ
                </a>
            </li>
            <li>
                <div class="flex items-center">
                    <svg class="w-6 h-6 text-gray-400" fill="currentColor" viewBox="0 0 20 20"
                         xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd"
                              d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                              clip-rule="evenodd"></path>
                    </svg>
                    <a class="ml-1 text-sm font-medium text-gray-700 hover:text-gray-900 md:ml-2 dark:text-gray-400 dark:hover:text-white">Thanh
                        toán</a>
                </div>
            </li>
        </ol>
    </nav>
    <section class="text-gray-700 body-font overflow-hidden bg-white">
        <div class="container px-5 py-8 mx-auto">
            <input type="hidden" id="id_product" value="{{$product->id}}">
            <div class="lg:w-4/5 mx-auto flex flex-wrap">
                <div class="lg:w-1/2 w-full object-cover object-center rounded">
                    <img alt="ecommerce" id="image_product" class="border border-gray-200"
                         src="/uploads/products/{{$product->image}}">
                </div>
                <div class="lg:w-1/2 w-full lg:pl-10 lg:py-6 mt-6 lg:mt-0">
                    <h1 class="text-gray-900 text-3xl title-font font-medium mb-1 fw-bold"
                        id="name_product">{{$product->name}}</h1>
                    <h4>Thương hiệu: <span id="brand_product">{{$product->brands ? $product->brands->name : ''}}</span>
                    </h4>
                    <p class="text-3xl text-gray-900"><span id="price_product">{{number_format($product->price)}}</span>đ</p>
                    <div class="leading-relaxed">
                        <ul>
                            <li style="font-weight: bold;list-style-type: square;margin-left: 15px">Tình trạng : Hàng
                                mới 100%
                            </li>
                            <li style="font-weight: bold;list-style-type: square;margin-left: 15px">Hình thức : Giao
                                hàng toàn quốc & thanh toán khi nhận hàng
                            </li>
                        </ul>
                    </div>
                    <div class="group_quantity">
                        @include('component.quantity')
                    </div>
                    <div class="mt-10">
                        <div class="flex items-center justify-between">
                            <h3 class="text-sm text-gray-900 font-medium">Size</h3>
                        </div>

                        <fieldset class="mt-4">
                            <legend class="sr-only">Choose a size</legend>
                            <div class="grid grid-cols-4 gap-4 sm:grid-cols-8 lg:grid-cols-4">
                                @foreach($product->sizes as $key => $size)
                                    <label for="rb_{{$size->id}}"
                                           id="radio_button_{{$size->id}}"
                                           onclick="chooseRadio({{$size->id}})"
                                           data-size="{{$size->size}}"
                                           class="{{$key == 0 ? 'active_radio_checkbox ' : ''}}radio_size group relative border rounded-md py-3 px-4 flex items-center justify-center text-sm font-medium uppercase hover:bg-gray-50 focus:outline-none sm:flex-1 sm:py-6 bg-white shadow-sm text-gray-900 cursor-pointer">
                                        <p id="">{{$size->size}}</p>
                                        <div class="absolute -inset-px rounded-md pointer-events-none"
                                             aria-hidden="true"></div>
                                    </label>
                                @endforeach
                            </div>
                        </fieldset>
                    </div>
                    <button type="button"
                            onclick="addToCart()"
                            class="mt-10 w-full bg-indigo-600 border border-transparent rounded-md py-3 px-8 flex items-center justify-center text-base font-medium text-white hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                        Add to bag
                    </button>
                </div>
            </div>

            <div class="product_detail" style="max-width: 80%; margin: 0px auto">
                <h1 style="padding: 30px;
                    font-size: 30px;
                    font-weight: bold;
                    text-align: center;">
                    Chi tiết sản phẩm
                </h1>
                {!! $product->desc !!}
            </div>
        </div>
        @include('component.relateProduct')
    </section>
    <style>
        .product_detail img{
            width: 1260px !important;
            height: 900px !important;
        }
        .active_radio_checkbox {
            border: 1px solid #555de5 !important;
        }
    </style>
@endsection
@section('js')
    <script src="/js/cart.js"></script>
@endsection
