<?php

namespace Tests\Feature\Products;

use App\Models\Product;
use Illuminate\Http\Response;
use Tests\TestCase;

class GetShowProductTest extends TestCase
{
    /** @test */
    public function unauthenticated_can_not_get_one_product()
    {
        $productCreated = $this->createFactoryProduct();
        $response = $this->get($this->getOneProductRoute($productCreated['id']));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect(route('login'));
    }

    /** @test */
    public function authenticated_with_permission_can_get_one_product()
    {
        $this->loginUserWithPermission('edit_product');
        $productCreated = $this->createFactoryProduct();
        $response = $this->get($this->getOneProductRoute($productCreated['id']));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('admin.product.edit');
        $response->assertSee($productCreated['name']);
    }

    /** @test */
    public function authenticated_with_permission_can_not_get_one_product_if_not_exists()
    {
        $this->loginUserWithPermission('show_product');
        $productId = -1;
        $response = $this->get($this->getOneProductRoute($productId));
        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }

    /** @test */
    public function authenticated_without_permission_can_not_get_one_product()
    {
        $this->loginUser();
        $productCreated = $this->createFactoryProduct();
        $response = $this->get($this->getOneProductRoute($productCreated['id']));
        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    public function createFactoryProduct()
    {
        return Product::factory()->create()->toArray();
    }

    protected function getOneProductRoute($id)
    {
        return route('products.show', $id);
    }
}
