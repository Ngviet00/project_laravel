<?php

namespace Tests\Feature\Categories;

use App\Models\Category;
use Illuminate\Http\Response;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class UpdateCategoryTest extends TestCase
{
    /** @test */
    public function authenticated_with_permission_can_update_category()
    {
        $this->loginUserWithPermission('update_category');
        $existsCategory = $this->createFactoryCategory();
        $dataUpdate = $this->makeFactoryCategory();
        $response = $this->putJson($this->updateCategoryRoute($existsCategory['id']), $dataUpdate);
        $response->assertStatus(Response::HTTP_OK);
        $response
            ->assertJson(fn(AssertableJson $json) => $json->where('status_code', Response::HTTP_OK)
                ->where('message', 'Update Category Success')
                ->etc()
            );
    }

    /** @test */
    public function authenticated_with_permission_can_not_update_category_if_name_is_null()
    {
        $this->loginUserWithPermission('update_category');
        $existsCategory = $this->createFactoryCategory();
        $dataUpdate = Category::factory()->make([
            'name' => null
        ])->toArray();
        $response = $this->putJson($this->updateCategoryRoute($existsCategory['id']), $dataUpdate);
        $response->assertJsonValidationErrors(['name']);
    }

    /** @test */
    public function authenticated_without_permission_can_not_update_category()
    {
        $this->loginUser();
        $existsCategory = $this->createFactoryCategory();
        $dataUpdate = $this->makeFactoryCategory();
        $response = $this->put($this->updateCategoryRoute($existsCategory['id']), $dataUpdate);
        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    /** @test */
    public function unauthenticated_can_not_update_category()
    {
        $existsCategory = $this->createFactoryCategory();
        $dataUpdate = $this->makeFactoryCategory();
        $response = $this->put($this->updateCategoryRoute($existsCategory['id']), $dataUpdate);
        $response->assertRedirect(route('login'));
    }

    public function createFactoryCategory(): array
    {
        return Category::factory()->create()->toArray();
    }

    public function makeFactoryCategory(): array
    {
        return Category::factory()->make()->toArray();
    }

    public function updateCategoryRoute($id): string
    {
        return route('categories.update', $id);
    }
}
