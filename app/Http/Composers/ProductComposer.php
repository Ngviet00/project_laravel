<?php

namespace App\Http\Composers;

use App\Repositories\ProductRepository;
use Illuminate\View\View;

class ProductComposer
{

    protected $productRepository;

    public function __construct(ProductRepository $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    public function compose(View $view)
    {
        $view->with('countProduct', $this->productRepository->count());
    }
}
