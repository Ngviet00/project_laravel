<?php

namespace App\Services;

use App\Repositories\BlogRepository;
use App\Traits\HandleImage;

/**
 * Class BlogService
 * @package App\Services
 */
class BlogService
{
    use HandleImage;

    protected BlogRepository $blogRepository;

    public function __construct(BlogRepository $blogRepository)
    {
        $this->blogRepository = $blogRepository;
    }

    public function all()
    {
        return $this->blogRepository->all();
    }

    public function create($request)
    {
        $data['image'] = $this->saveImage($request);
        $data['title'] = $request->title;
        $data['short_desc'] = $request->short_desc;
        $data['description'] = $request->desc_detail;
        return $this->blogRepository->create($data);
    }

    public function findOrFail($id)
    {
        return $this->blogRepository->getById($id);
    }

    public function update($request, $id)
    {
        $dataUpdate = $request->all();

        $blog = $this->blogRepository->getById($id);

        $dataUpdate['image'] = $this->updateImage($request, $blog->image);
        $dataUpdate['title'] = $request->title;
        $dataUpdate['short_desc'] = $request->short_desc;
        $dataUpdate['description'] = $request->desc_detail;

        $blog->update($dataUpdate);

        return $blog;
    }

    public function delete($id)
    {
        $blog = $this->blogRepository->getById($id);

        $this->deleteImage($blog->image);

        $blog->delete();

        return $blog;
    }
}
