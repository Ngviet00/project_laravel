@extends('admin.layouts.app')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card my-4">
                    <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
                        <div class="bg-gradient-primary shadow-primary border-radius-lg pt-4 pb-3">
                            <h6 class="text-white text-capitalize ps-3">Product</h6>
                        </div>
                    </div>
                    <div class="pt-3 d-flex justify-content-between" style="margin-left: 1.2%;">
                        <div>
                            <div class="input-group">
                                <form class="form-search-product">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <div class="input-group input-group-outline my-3 mb-0">
                                                <select name="categoryId" id="category_id" class="form-control"
                                                        style="box-shadow: none;height: 45px">
                                                    <option value="" selected>Danh mục</option>
                                                    @foreach($parentCategory as $category)
                                                        <option
                                                            value="{{$category->id}}" {{ request('category_id') == $category->id ? 'selected' : '' }}>
                                                            {{ $category->name }}
                                                        </option>
                                                        @foreach($category->childrenCategory as $children)
                                                            <option
                                                                value="{{$children->id}}" {{ request('category_id') == $children->id ? 'selected' : '' }}>
                                                                -----{{ $children->name }}
                                                            </option>
                                                        @endforeach
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="my-3 mb-0">
                                                <input type="text" class="form-control shadow-none" type="search"
                                                       id="name" name="name" value="{{ request('name') }}"
                                                       placeholder="Name"><br>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <span class="text-dark fw-bold"
                                                  style="position: absolute; margin-top: -10px;">Price</span>
                                            <div class="my-3 mb-0">
                                                <input type="text" class="form-control shadow-none" type="search"
                                                       id="min_price" name="minPrice"
                                                       value="{{ request('min_price') }}" placeholder="From"><br>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="my-3 mb-0">
                                                <input type="text" class="form-control shadow-none" type="search"
                                                       id="max_price" name="maxPrice"
                                                       value="{{ request('max_price') }}" placeholder="To"><br>
                                            </div>
                                        </div>

                                        <div class="col-md-1">
                                            <button class="btn btn-primary btn-search-product" type="submit"
                                                    style="margin-top: 17px;">
                                                <i class="fas fa-search"></i>
                                            </button>
                                        </div>
                                        <div class="col-md-1">
                                            <p class="text-dark text-decoration-underline fw-bold mt-4 cursor-pointer btn-delete-search">
                                                Xóa</p>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                        @hasPermission('create_product')
                        <div>
                            <a href="{{route('products.create')}}" style="margin-right: 17px;margin-top: 17px;" class="btn bg-gradient-primary text-capitalize btn-show-modal-create-product">Create</a>
                        </div>
                        @endhasPermission

                        @hasPermission('edit_product')
                        <div class="modal fade" id="modal-edit-product" tabindex="-1" role="dialog"
                             aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document" style="max-width: 800px;">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title font-weight-normal" id="exampleModalLabel">Update
                                            Product</h5>
                                        <button type="button" class="btn-close text-dark" data-bs-dismiss="modal"
                                                aria-label="Close">
                                        </button>
                                    </div>
                                    <div class="modal-body edit-form-product">
                                        ...
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn bg-gradient-secondary" data-bs-dismiss="modal">
                                            Close
                                        </button>
                                        <button type="button" class="btn bg-gradient-primary btn-update-product">
                                            Update
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endhasPermission
                    </div>
                    <div id="list" data-action="{{ route('products.list') }}">
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script src="/js/admin/product.js"></script>
@endsection

