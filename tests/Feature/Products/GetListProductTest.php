<?php

namespace Tests\Feature\Products;

use App\Models\Product;
use Illuminate\Http\Response;
use Tests\TestCase;

class GetListProductTest extends TestCase
{
    /** @test */
    public function unauthenticated_can_not_get_list_product()
    {
        $response = $this->get($this->getListProductRoute());
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect(route('login'));
    }

    /** @test */
    public function authenticated_with_permission_can_get_list_product()
    {
        $this->loginUserWithPermission('show_product');
        $response = $this->get($this->getListProductRoute());
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('admin.product.index');
    }

    /** @test */
    public function authenticated_without_permission_can_not_get_list_product()
    {
        $this->loginUser();
        $response = $this->get($this->getListProductRoute());
        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    protected function getListProductRoute()
    {
        return route('products.index');
    }
}
