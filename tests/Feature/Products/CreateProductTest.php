<?php

namespace Tests\Feature\Products;

use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Response;
use Illuminate\Http\UploadedFile;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class CreateProductTest extends TestCase
{

    /** @test */
    public function unauthenticated_can_not_create_product()
    {
        $dataCreate = $this->makeFactoryProduct();
        $response = $this->post($this->storeProductRoute(), $dataCreate);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect(route('login'));
    }

    /** @test */
    public function authenticated_without_permission_can_not_create_new_product()
    {
        $this->loginUser();
        $dataCreate = $this->makeFactoryProduct();
        $response = $this->post($this->storeProductRoute(), $dataCreate);
        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    /** @test */
    public function authenticated_with_permission_can_not_create_new_product_if_name_is_null()
    {
        $this->loginUserWithPermission('store_product');
        $dataCreate = $this->makeFactoryProductSetFieldNull('name');
        $response = $this->postJson($this->storeProductRoute(), $dataCreate);
        $response->assertJsonValidationErrors(['name']);
    }

    /** @test */
    public function authenticated_with_permission_can_not_create_new_product_if_price_is_null()
    {
        $this->loginUserWithPermission('store_product');
        $dataCreate = $this->makeFactoryProductSetFieldNull('price');
        $response = $this->postJson($this->storeProductRoute(), $dataCreate);
        $response->assertJsonValidationErrors(['price']);
    }

    /** @test */
    public function authenticated_with_permission_can_not_create_new_product_if_price_is_not_number()
    {
        $this->loginUserWithPermission('store_product');
        $dataCreate = $this->makeFactoryProductSetFieldNull('price', 'abc');
        $response = $this->postJson($this->storeProductRoute(), $dataCreate);
        $response->assertJsonValidationErrors(['price']);
    }

    /** @test */
    public function authenticated_with_permission_can_not_create_new_product_if_desc_is_null()
    {
        $this->loginUserWithPermission('store_product');
        $dataCreate = $this->makeFactoryProductSetFieldNull('desc');
        $response = $this->postJson($this->storeProductRoute(), $dataCreate);
        $response->assertJsonValidationErrors(['desc']);
    }

    /** @test */
    public function authenticated_with_permission_can_not_create_new_product_if_image_is_invalid()
    {
        $this->loginUserWithPermission('store_product');
        $image = UploadedFile::fake()->create('image.pdf', 100);
        $dataCreate = Product::factory()->make(['image' => $image])->toArray();
        $response = $this->postJson($this->storeProductRoute(), $dataCreate);
        $response->assertJsonValidationErrors(['image']);
    }

    /** @test */
    public function authenticated_with_permission_can_create_new_product_if_data_is_valid()
    {
        $this->loginUserWithPermission('store_product');
        $dataCreate = $this->makeFactoryProduct();
        $response = $this->postJson($this->storeProductRoute(), $dataCreate);
        $response->assertStatus(Response::HTTP_OK);
        $response->assertJson(fn(AssertableJson $json) => $json->has('data',
            fn(AssertableJson $json) => $json->where('name', $dataCreate['name'])->etc()
        )->etc()
        );
        $this->assertDatabaseHas('products', [
            'name' => $dataCreate['name'],
            'price' => $dataCreate['price']
        ]);
    }

    public function storeProductRoute()
    {
        return route('products.store');
    }

    public function getCreateProductRoute()
    {
        return route('products.create');
    }

    public function makeFactoryProduct()
    {
        return Product::factory()->make()->toArray();
    }

    public function makeFactoryProductSetFieldNull($field, $value = null): array
    {
        return Product::factory()->make([
            $field => $value
        ])->toArray();
    }
}
