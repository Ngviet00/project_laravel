<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use \App\Http\Controllers\CartController;

Route::get('/', [HomeController::class, 'index'])->name('hClient');

Route::group(['middleware' => 'auth', 'prefix' => 'admin'], function () {
    include("categoryRoute.php");
    include("productRoute.php");
    include("userRoute.php");
    include("roleRoute.php");
    include("slideRoute.php");
    include('blogRoute.php');
    include("brandRoute.php");
    include("sizeRoute.php");
    Route::get('detail-order/{id}', [HomeController::class, 'detailOrder'])->name('detailOrder');
    Route::get('list-order', [HomeController::class, 'listOrder'])->name('listOrder');
    Route::get('update-order/{id}/{value}', [HomeController::class, 'updateOrder'])->name('updateOrder');
});

Auth::routes();
Route::get('/admin/home', [App\Http\Controllers\HomeController::class, 'dashboard'])->name('home');

Auth::routes();

//client
Route::get('/product/{product}', [HomeController::class, 'show'])->name('show');
Route::get('/checkout', [HomeController::class, 'checkout'])->name('checkout');

Route::get('/cart', [CartController::class, 'index'])->name('cart');
Route::post('addToCart', [CartController::class, 'addCart'])->name('addCart');
Route::get('deleteCart/{id}', [CartController::class, 'deleteCart'])->name('deleteCart');
Route::get('deleteAllCart', [CartController::class, 'deleteAllCart'])->name('deleteAllCart');
Route::post('updateCart', [CartController::class, 'updateCart'])->name('updateCart');

Route::get('blog/{id}', [HomeController::class, 'blog'])->name('blog');
Route::match(array('GET', 'POST'), '/filter_product', [HomeController::class, 'filterProduct'])->name('filterProduct');

Route::post('order', [HomeController::class, 'order'])->name('order');

Route::get('history-order', [HomeController::class, 'historyOrder'])->name('list-order');
Route::get('history-order/login', [HomeController::class, 'loginOrder'])->name('history-order');

Route::get('logoutPhone', [HomeController::class, 'logoutPhone'])->name('logoutPhone');



