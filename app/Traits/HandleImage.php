<?php

namespace App\Traits;

use Intervention\Image\Facades\Image;

trait HandleImage
{
    protected $path = 'uploads/products/';
    protected $imageDefault = 'default.png';

    public function verifyImage($request)
    {
        return $request->hasFile('image');
    }

    public function saveImage($request)
    {
        if ($this->verifyImage($request)) {
            $file = $request->file('image');
            $name = time() . '.' . $file->getClientOriginalExtension();
            $location = public_path($this->path . $name);
            Image::make($file)->save($location);
            return $name;
        }
        return $this->imageDefault;
    }

    public function updateImage($request, $currentImage)
    {
        if ($this->verifyImage($request)) {
            $this->deleteImage($currentImage);
            return $this->saveImage($request);
        }
        return $currentImage;
    }

    public function deleteImage($imageName)
    {
        $pathName = $this->path . $imageName;
        if (file_exists($pathName) && $imageName != $this->imageDefault) {
            unlink($pathName);
        }
    }
}
