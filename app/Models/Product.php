<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'price', 'desc', 'image', 'brand_id', 'category_id'];

    public function categories()
    {
        return $this->belongsTo(Category::class, 'category_id', 'id');
    }

    public function scopeWithName($query, $name)
    {
        return $name ? $query->where('name', 'like', "%" . $name . "%") : null;
    }

    public function scopeWithMinPrice($query, $minPrice)
    {
        return $minPrice ? $query->where('price', '>', $minPrice) : null;
    }

    public function scopeWithMaxPrice($query, $maxPrice)
    {
        return $maxPrice ? $query->where('price', '<', $maxPrice) : null;
    }

    public function scopeWithCategoryId($query, $categoryId)
    {
        return $categoryId ? $query->where('category_id', $categoryId) : null;
    }

    public function attachCategory($categoryId)
    {
        return $this->categories()->attach($categoryId);
    }

    public function syncCategory($categoryId)
    {
        return $this->categories()->sync($categoryId);
    }

    public function detachCategory()
    {
        return $this->categories()->detach();
    }

    public function sizes()
    {
        return $this->belongsToMany(Size::class);
    }

    public function brands(){
        return $this->belongsTo(Brand::class, 'brand_id', 'id');
    }
}
