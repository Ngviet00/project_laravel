<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('permissions')->insert([
            ['name' => 'view_user', 'group_name' => 'user'],
            ['name' => 'create_user', 'group_name' => 'user'],
            ['name' => 'store_user', 'group_name' => 'user'],
            ['name' => 'edit_user', 'group_name' => 'user'],
            ['name' => 'update_user', 'group_name' => 'user'],
            ['name' => 'delete_user', 'group_name' => 'user'],
            ['name' => 'show_user', 'group_name' => 'user'],

            ['name' => 'view_role', 'group_name' => 'role'],
            ['name' => 'create_role', 'group_name' => 'role'],
            ['name' => 'store_role', 'group_name' => 'role'],
            ['name' => 'edit_role', 'group_name' => 'role'],
            ['name' => 'update_role', 'group_name' => 'role'],
            ['name' => 'delete_role', 'group_name' => 'role'],
            ['name' => 'show_role', 'group_name' => 'role'],

            ['name' => 'view_product', 'group_name' => 'product'],
            ['name' => 'create_product', 'group_name' => 'product'],
            ['name' => 'store_product', 'group_name' => 'product'],
            ['name' => 'edit_product', 'group_name' => 'product'],
            ['name' => 'update_product', 'group_name' => 'product'],
            ['name' => 'delete_product', 'group_name' => 'product'],
            ['name' => 'show_product', 'group_name' => 'product'],

            ['name' => 'view_category', 'group_name' => 'category'],
            ['name' => 'create_category', 'group_name' => 'category'],
            ['name' => 'store_category', 'group_name' => 'category'],
            ['name' => 'edit_category', 'group_name' => 'category'],
            ['name' => 'update_category', 'group_name' => 'category'],
            ['name' => 'delete_category', 'group_name' => 'category'],
            ['name' => 'show_category', 'group_name' => 'category']
        ]);
    }
}
