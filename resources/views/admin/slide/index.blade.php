@extends('admin.layouts.app')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card my-4">
                    <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
                        <div class="bg-gradient-primary shadow-primary border-radius-lg pt-4 pb-3">
                            <h6 class="text-white text-capitalize ps-3">Slide</h6>
                        </div>
                    </div>

                    <div class="pt-3 d-flex justify-content-between">
                        <div>
                            @if(Session::has('message'))
                                <p class="fw-bold text-success pt-2"
                                   style="padding-left:17px">{!! Session::get('message') !!}
                                </p>
                            @endif
                        </div>
                        <a href="{{route('slides.create')}}" class="btn btn-danger d-inline-block text-capitalize"
                           style="margin-right:17px">Create</a>
                    </div>

                    <div class="px-0 pb-2">
                        <div class="table-responsive p-0">
                            <table class="table align-items-center mb-0" style="table-layout: fixed; width: 100%">
                                <thead>
                                <tr>
                                    <th width="70"
                                        class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                        ID
                                    </th>
                                    <th width="220"
                                        class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
                                        Hinh anh
                                    </th>
                                    <th width="300"
                                        class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                                        Hanh dong
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($listSlide as $item)
                                    <tr>
                                        <td style="display:table-cell;vertical-align:top;height: 30px">
                                            <div style="padding-left: 20px; height: 100%"
                                                 class="text-sm font-weight-bold mb-0">
                                                {{$item->id}}
                                            </div>
                                        </td>
                                        <td style="display:table-cell;vertical-align:top;height: 30px">
                                            <div class="text-sm font-weight-bold mb-0"
                                                 style="height: 100%">
                                                <img
                                                    src="/uploads/products/{{$item->name}}"
                                                    width="150px">
                                            </div>
                                        </td>
                                        <td class="align-middle d-flex justify-content-around align-items-center pt-3"
                                            style="height: 118px">

                                            @hasPermission('edit_role')
                                            <a href="{{ route('slides.edit', $item->id) }}"
                                               class="text-secondary mr-2 font-weight-bold text-sm d-inline-block text-decoration-none">
                                                <i class="fas fa-edit"></i>
                                            </a>
                                            @endhasPermission

                                            @hasPermission('delete_role')
                                            <form action="{{route('slides.delete', $item->id)}}" method="POST">
                                                @csrf
                                                @method('DELETE')
                                                <button class='btn m-0 text-sm'
                                                        onclick="return confirm('Are you sure?')">
                                                    <i class="fas fa-trash-alt text-secondary"></i>
                                                </button>
                                            </form>
                                            @endhasPermission
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            @if(count($listSlide) == 0)
                                <div class="py-3" style="margin-left: 40%;">
                                    <p style="margin-left: 22px !important;" class="m-0 fw-bold text-danger">No Data</p>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <style>
        .switch {
            position: relative;
            display: inline-block;
            width: 60px;
            height: 34px;
        }

        .switch input {
            opacity: 0;
            width: 0;
            height: 0;
        }

        .slider {
            position: absolute;
            cursor: pointer;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: #ccc;
            -webkit-transition: .4s;
            transition: .4s;
        }

        .slider:before {
            position: absolute;
            content: "";
            height: 26px;
            width: 26px;
            left: 4px;
            bottom: 4px;
            background-color: white;
            -webkit-transition: .4s;
            transition: .4s;
        }

        input:checked + .slider {
            background-color: #2196F3;
        }

        input:focus + .slider {
            box-shadow: 0 0 1px #2196F3;
        }

        input:checked + .slider:before {
            -webkit-transform: translateX(26px);
            -ms-transform: translateX(26px);
            transform: translateX(26px);
        }

        /* Rounded sliders */
        .slider.round {
            border-radius: 34px;
        }

        .slider.round:before {
            border-radius: 50%;
        }
    </style>
@endsection
@section('js')
    <script>
        function handleChangeStatus(ele)
        {
            let id= $(ele).attr('data-id');

            console.log(id);
            // console.log(id);
        }
    </script>
@endsection

