<?php

namespace Tests\Feature\Users;

use App\Models\User;
use Illuminate\Http\Response;
use Tests\TestCase;

class LoginUserTest extends TestCase
{

    /** @test */
    public function user_can_view_form_login()
    {
        $response = $this->get('/register');
        $response->assertStatus(Response::HTTP_OK);
    }

    /** @test */
    public function user_can_login_if_data_is_valid()
    {
        $user = $this->createFactoryUser();
        $response = $this->post(route('login'), [
            'email' => $user['email'],
            'password' => '12345678'
        ]);
        $response->assertStatus(Response::HTTP_FOUND);
        $this->assertAuthenticated();
    }

    /** @test */
    public function user_can_not_login_if_data_is_incorrect()
    {
        $user = $this->createFactoryUser();
        $response = $this->post(route('login'), [
            'email' => $user['email'],
            'password' => 'abc123wq43'
        ]);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors();
        $this->assertGuest();
    }

    /** @test */
    public function user_logout()
    {
        $user = User::factory()->create();
        $response = $this->actingAs($user)->post('/logout');
        $response->assertStatus(Response::HTTP_FOUND);
        $this->assertGuest();
    }

    /** @test */
    public function user_can_not_login_if_email_is_null()
    {
        $user = $this->createFactoryUser();
        $user['email'] = null;
        $response = $this->post(route('login'), $user);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['email']);
    }

    /** @test */
    public function user_can_not_login_if_password_is_null()
    {
        $user = $this->createFactoryUser();
        $user['password'] = null;
        $response = $this->post(route('login'), $user);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['password']);
    }

    public function createFactoryUser(): array
    {
        return User::factory()->create()->toArray();
    }
}
