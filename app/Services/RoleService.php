<?php

namespace App\Services;

use App\Repositories\RoleRepository;

class RoleService
{
    protected $roleRepository;

    public function __construct(RoleRepository $roleRepository)
    {
        $this->roleRepository = $roleRepository;
    }

    public function search($request)
    {
        $dataSearch = $request->all();

        $dataSearch['name'] = $request->name ?? '';

        return $this->roleRepository->search($dataSearch);
    }

    public function create($request)
    {
        $dataCreate = $request->all();

        $permissions = $request->permissions ?? [];

        $role = $this->roleRepository->create($dataCreate);

        $role->attachPermission($permissions);

        return $role;
    }

    public function find($id)
    {
        return $this->roleRepository->findOrFail($id);
    }

    public function delete($id)
    {
        $role = $this->roleRepository->findOrFail($id);

        $role->detachPermission();

        $role->delete();

        return $role;
    }

    public function update($request, $id)
    {
        $dataUpdate = $request->all();

        $role = $this->roleRepository->findOrFail($id);

        $permissions = $request->permissions ?? [];

        $role->syncPermission($permissions);

        $role->update($dataUpdate);

        return $role;
    }

    public function count()
    {
        return $this->roleRepository->count();
    }
}
