@if(count($relateProduct) > 0)
    <div class="bg-white">
        <h4 class="text-3xl text-left py-3" style="margin-left: 8rem;">Sản phẩm khác</h4>
        <div class="max-w-2xl mx-auto px-4 sm:py-24 sm:px-6 lg:max-w-7xl lg:px-8"
             style="padding-bottom: 50px;padding-top: 0px;">
            <h2 class="sr-only">Products</h2>
            <div class="grid grid-cols-1 gap-y-10 sm:grid-cols-2 gap-x-6 lg:grid-cols-3 xl:grid-cols-4 xl:gap-x-8">
                @foreach($relateProduct as $itemRelate)
                    <a href="{{$itemRelate->id}}" class="group">
                        <div
                            class="w-full aspect-w-1 aspect-h-1 bg-gray-200 rounded-lg overflow-hidden xl:aspect-w-7 xl:aspect-h-8">
                            <img
                                style="width: 254px !important;height: 200px !important;margin: 0px auto"
                                src="/uploads/products/{{$itemRelate->image}}"
                                alt="Tall slender porcelain bottle with natural clay textured body and cork stopper."
                                class="w-full h-full object-center object-cover group-hover:opacity-75">
                        </div>
                        <h3 class="mt-4 text-sm text-gray-700">{{$itemRelate->name}}</h3>
                        <p class="mt-1 text-lg font-medium text-gray-900">{{ number_format($itemRelate->price)}} đ</p>
                    </a>
                @endforeach
            </div>
        </div>
    </div>
@endif
