@extends('admin.layouts.app')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card my-4">
                    <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
                        <div class="bg-gradient-primary shadow-primary border-radius-lg pt-4 pb-3">
                            <h6 class="text-white text-capitalize ps-3">Create New Role</h6>
                        </div>
                    </div>

                    <div class="px-0 pb-2">
                        <form method="POST" action="{{route('roles.store')}}" class="p-4">
                            @csrf
                            <div class="col-xs-12 col-sm-12 col-md-12 mt-3">
                                <div class="form-group custom-form-group-create">
                                    <strong class="form-check">Name: </strong>
                                    <div class="form-check input-group input-group-dynamic info-horizontal">
                                        <input type="text" name="name" id="name" class="form-control shadow-none"
                                               style="outline: none" value="{{old('name')}}" onfocus="focused(this)"
                                               onfocusout="defocused(this)">
                                    </div>
                                    @error('name')
                                    <div class="text-danger my-1">{{ $message }} </div> @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="text-lg text-bold mt-2 text-dark">Permission</label><br>
                                <div class="pl-2 ml-3 d-flex align-items-center">
                                    <input type="checkbox" id="check" class="select_all"
                                           style="margin-left: 5px;margin-right:10px">
                                    <label for="check" class="m-0 ml-3 d-inline-block fw-bold text-dark"
                                           style="font-size: 17px">Chon
                                        tat ca</label>
                                </div>
                                @foreach($groupPermissions as $group => $permission)
                                    <p class="fw-bold text-dark m-0 text-capitalize mt-1"
                                       style="padding-left: 20px;">{{ $group }}</p>
                                    @foreach($permission as $item)
                                        <div class="d-inline-block mb-3">
                                            <div class="form-check d-flex align-items-center">
                                                <input type="checkbox" value="{{ $item->id }}" name="permissions[]"
                                                       id="{{$item->id}}"
                                                       class="checkbox orm-input @error('permissions') is-invalid @enderror">
                                                <label for="{{ $item->id }}" class="m-0 text-capitalize text-dark"
                                                       style="display:block;margin-left:5px !important;font-size:16px">{{ $item->name }}</label>
                                            </div>
                                        </div>
                                    @endforeach
                                @endforeach
                            </div>
                            <a href="{{route('roles.index')}}" class="mt-4 btn btn-dark">Back</a>
                            <button type="submit" class=" mt-4 btn btn-danger">Add</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script src="/js/admin/role.js"></script>
@endsection
