<?php

use App\Http\Controllers\CategoryController;
use Illuminate\Support\Facades\Route;

Route::name('categories.')->prefix('categories')->group(function () {
    Route::get('/', [CategoryController::class, 'index'])
        ->name('index')
        ->middleware('permission:view_category');

    Route::get('/list', [CategoryController::class, 'list'])
        ->name('list')
        ->middleware('permission:view_category');

    Route::get('/create', [CategoryController::class, 'create'])
        ->name('create')
        ->middleware('permission:create_category');

    Route::post('/', [CategoryController::class, 'store'])
        ->name('store')
        ->middleware('permission:store_category');

    Route::get('/{id}', [CategoryController::class, 'show'])
        ->name('show')
        ->middleware('permission:show_category');

    Route::get('/edit/{id}', [CategoryController::class, 'edit'])
        ->name('edit')
        ->middleware('permission:edit_category');

    Route::put('/{category}', [CategoryController::class, 'update'])
        ->name('update')
        ->middleware('permission:update_category');

    Route::delete('/{id}', [CategoryController::class, 'destroy'])
        ->name('delete')
        ->middleware('permission:delete_category');
});
