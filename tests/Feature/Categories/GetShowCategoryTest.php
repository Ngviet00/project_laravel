<?php

namespace Tests\Feature\Categories;

use App\Models\Category;
use Illuminate\Http\Response;
use Tests\TestCase;

class GetShowCategoryTest extends TestCase
{
    /** @test */
    public function unauthenticated_can_not_see_one_category()
    {
        $categoryCreated = $this->createFactoryCategory();
        $response = $this->get($this->getShowCategoryRoute($categoryCreated['id']));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect(route('login'));
    }

    /** @test */
    public function authenticated_with_permission_can_see_one_category()
    {
        $this->loginUserWithPermission('show_category');
        $categoryCreated = $this->createFactoryCategory();
        $response = $this->get($this->getShowCategoryRoute($categoryCreated['id']));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('admin.category.edit');
        $response->assertSee($categoryCreated['name']);
    }

    /** @test */
    public function authenticated_with_permission_can_not_get_one_category_if_not_exists()
    {
        $this->loginUserWithPermission('show_category');
        $categoryId = -1;
        $response = $this->get($this->getShowCategoryRoute($categoryId));
        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }

    /** @test */
    public function authenticated_without_permission_can_not_see_one_category()
    {
        $this->loginUser();
        $categoryCreated = $this->createFactoryCategory();
        $response = $this->get($this->getShowCategoryRoute($categoryCreated['id']));
        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    public function getShowCategoryRoute($id): string
    {
        return route('categories.show', $id);
    }

    public function createFactoryCategory(): array
    {
        return Category::factory()->create()->toArray();
    }
}
