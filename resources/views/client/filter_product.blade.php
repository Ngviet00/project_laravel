@extends('layouts.app_client')
@php
    $arrPrice = [
        [
            'id' => 1,
            'label' => '<1.000.000'
        ],
        [
            'id' => 2,
            'label' => '1.000.000 đến 2.000.000'
        ],
        [
            'id' => 3,
            'label' => '2.000.000 đến 5.000.000'
        ],
        [
            'id' => 4,
            'label' => '>5.000.000'
        ]
    ];
@endphp
@section('content')
    <div class="bg-white">
        <nav class="flex py-6 bg-white" aria-label="Breadcrumb" style="padding-left: 8rem;">
            <ol class="inline-flex items-center space-x-1 md:space-x-3">
                <li class="inline-flex items-center">
                    <a href="/"
                       class="inline-flex items-center text-sm text-gray-700 hover:text-gray-900 dark:text-gray-400 dark:hover:text-white">
                        <svg class="mr-2 w-4 h-4" fill="currentColor" viewBox="0 0 20 20"
                             xmlns="http://www.w3.org/2000/svg">
                            <path
                                d="M10.707 2.293a1 1 0 00-1.414 0l-7 7a1 1 0 001.414 1.414L4 10.414V17a1 1 0 001 1h2a1 1 0 001-1v-2a1 1 0 011-1h2a1 1 0 011 1v2a1 1 0 001 1h2a1 1 0 001-1v-6.586l.293.293a1 1 0 001.414-1.414l-7-7z"></path>
                        </svg>
                        Trang chủ
                    </a>
                </li>
                <li>
                    <div class="flex items-center">
                        <svg class="w-6 h-6 text-gray-400" fill="currentColor" viewBox="0 0 20 20"
                             xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd"
                                  d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                  clip-rule="evenodd"></path>
                        </svg>
                        <a class="ml-1 text-sm font-medium text-gray-700 hover:text-gray-900 md:ml-2 dark:text-gray-400 dark:hover:text-white">Products</a>
                    </div>
                </li>
            </ol>
        </nav>
        <div>
            <main class="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
                <div class="relative z-10 flex items-baseline justify-between pt-4 pb-6 border-b border-gray-200">
                    <h1 class="text-4xl font-extrabold tracking-tight text-gray-900">
                        Lọc sản phẩm
                        <a href="{{route('filterProduct')}}" style="font-size: 16px;margin-left: 2rem;"
                           class="text-danger text-decoration-underline">Xóa tìm kiếm</a>
                    </h1>

                </div>

                <section aria-labelledby="products-heading" class="pt-6 pb-24">
                    <h2 id="products-heading" class="sr-only">Products</h2>

                    <div class="grid grid-cols-1 lg:grid-cols-4 gap-x-8 gap-y-10">
                        <form class="hidden lg:block" method="GET" action="{{route('filterProduct')}}">
                            <div class="border-b border-gray-200 py-6">
                                <h3 class="-my-3 flow-root">
                                    <button type="button"
                                            class="py-3 bg-white w-full flex items-center justify-between text-sm text-gray-400 hover:text-gray-500"
                                            aria-controls="filter-section-2" aria-expanded="false">
                                        <span class="font-medium text-gray-900 fw-bold"> Danh mục sản phẩm </span>
                                    </button>
                                </h3>
                                <div class="pt-6" id="filter-section-2">
                                    <div class="space-y-4">
                                        @foreach($categories as $category)
                                            <div class="flex items-center">
                                                <input id="filter-category-{{$category->id}}" name="category"
                                                       value="{{$category->id}}"
                                                       type="radio"
                                                       {{ isset($listRequests['category']) && $listRequests['category'] == $category->id ? 'checked' : '' }}
                                                       class="h-4 w-4 border-gray-300 rounded text-indigo-600 focus:ring-indigo-500">
                                                <label for="filter-category-{{$category->id}}"
                                                       class="ml-3 text-sm text-gray-600"> {{$category->name}} </label>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>

                            <div class="border-b border-gray-200 py-6">
                                <h3 class="-my-3 flow-root">
                                    <button type="button"
                                            class="py-3 bg-white w-full flex items-center justify-between text-sm text-gray-400 hover:text-gray-500"
                                            aria-controls="filter-section-1" aria-expanded="false">
                                        <span class="font-medium text-gray-900"> Sắp xếp </span>
                                    </button>
                                </h3>
                                <div class="pt-6" id="filter-section-1">
                                    <div class="space-y-4">
                                        <div class="flex items-center">
                                            <input id="filter-sort-asc" name="sort"
                                                   value="asc"
                                                   type="radio"
                                                   {{ isset($listRequests['sort']) && $listRequests['sort'] == 'asc' ? 'checked' : '' }}
                                                   class="h-4 w-4 border-gray-300 rounded text-indigo-600 focus:ring-indigo-500">
                                            <label for="filter-sort-asc"
                                                   class="ml-3 text-sm text-gray-600"> Tăng dần </label>
                                        </div>
                                        <div class="flex items-center">
                                            <input id="filter-sort-desc" name="sort"
                                                   value="desc"
                                                   {{ isset($listRequests['sort']) && $listRequests['sort'] == 'desc' ? 'checked' : '' }}
                                                   type="radio"
                                                   class="h-4 w-4 border-gray-300 rounded text-indigo-600 focus:ring-indigo-500">
                                            <label for="filter-sort-desc"
                                                   class="ml-3 text-sm text-gray-600"> Giảm dần </label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="border-b border-gray-200 py-6">
                                <h3 class="-my-3 flow-root">
                                    <button type="button"
                                            class="py-3 bg-white w-full flex items-center justify-between text-sm text-gray-400 hover:text-gray-500"
                                            aria-controls="filter-section-1" aria-expanded="false">
                                        <span class="font-medium text-gray-900"> Thương hiệu </span>
                                    </button>
                                </h3>
                                <div class="pt-6" id="filter-section-1">
                                    <div class="space-y-4">
                                        @foreach($brands as $brand)
                                            <div class="flex items-center">
                                                <input id="filter-brand-{{$brand->id}}" name="brands"
                                                       value="{{$brand->id}}"
                                                       type="radio"
                                                       {{ isset($listRequests['brands']) && $listRequests['brands'] == $brand->id ? 'checked' : '' }}
                                                       class="h-4 w-4 border-gray-300 rounded text-indigo-600 focus:ring-indigo-500">
                                                <label for="filter-brand-{{$brand->id}}"
                                                       class="ml-3 text-sm text-gray-600"> {{$brand->name}} </label>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>

                            <div class="border-b border-gray-200 py-6">
                                <h3 class="-my-3 flow-root">
                                    <button type="button"
                                            class="py-3 bg-white w-full flex items-center justify-between text-sm text-gray-400 hover:text-gray-500"
                                            aria-controls="filter-section-1" aria-expanded="false">
                                        <span class="font-medium text-gray-900"> Giá </span>
                                    </button>
                                </h3>
                                <div class="pt-6" id="filter-section-1">
                                    <div class="space-y-4">
                                        @foreach($arrPrice as $price)
                                            <div class="flex items-center">
                                                <div class="flex items-center">
                                                    <input id="filter-price-{{$price['id']}}" name="price"
                                                           value="{{$price['id']}}"
                                                           type="radio"
                                                           {{ isset($listRequests['price']) && $listRequests['price'] == $price['id'] ? 'checked' : '' }}
                                                           class="h-4 w-4 border-gray-300 rounded text-indigo-600 focus:ring-indigo-500">
                                                    <label for="filter-price-{{$price['id']}}"
                                                           class="ml-3 text-sm text-gray-600"> {{$price['label']}} </label>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>

                            <button class="mt-3 bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded">
                                Tìm kiếm
                            </button>
                        </form>
                        <div class="lg:col-span-3">
                            <div class="d-flex flex-wrap" style="justify-content: space-around">
                                @forelse($products as $pro)
                                    <div style="flex-basis: 31%">
                                        <a href="product/{{$pro->id}}" class="group">
                                            <div
                                                class="w-full aspect-w-1 aspect-h-1 bg-gray-200 rounded-lg overflow-hidden xl:aspect-w-7 xl:aspect-h-8">
                                                <img
                                                    src="/uploads/products/{{$pro->image}}"
                                                    alt="Tall slender porcelain bottle with natural clay textured body and cork stopper."
                                                    class="w-full h-full object-center object-cover group-hover:opacity-75">
                                            </div>
                                            <h3 class="mt-4 text-sm text-gray-700">{{$pro->name}}</h3>
                                            <p class="mt-1 text-lg font-medium text-gray-900">{{number_format($pro->price)}}
                                                đ</p>
                                        </a>
                                    </div>
                                @empty
                                    <p class="text-center justify-content-center" style="font-weight: bold">No data</p>
                                @endforelse
                            </div>
                        </div>
                    </div>
                </section>
            </main>
        </div>
    </div>
@endsection
