<?php

namespace App\Repositories;

use App\Models\Slide;
use JasonGuru\LaravelMakeRepository\Repository\BaseRepository;

/**
 * Class SlideRepository.
 */
class SlideRepository extends BaseRepository
{
    /**
     * @return string
     *  Return the model
     */
    public function model()
    {
        return Slide::class;
    }
}
