<?php

namespace Tests\Feature\Products;

use App\Models\Product;
use Illuminate\Http\Response;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class DeleteProductTest extends TestCase
{
    /** @test */
    public function unauthenticated_can_not_delete_product()
    {
        $productCreated = $this->createFactoryProduct();
        $response = $this->delete($this->deleteProductRoute($productCreated['id']));
        $response->assertRedirect(route('login'));
    }

    /** @test */
    public function authenticated_with_permission_can_delete_product()
    {
        $this->loginUserWithPermission('delete_product');
        $productCreated = $this->createFactoryProduct();
        $response = $this->deleteJson($this->deleteProductRoute($productCreated['id']));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertJson(fn(AssertableJson $json) =>
            $json->where('message', 'Delete Product Success')
            ->etc()
        );
        $this->assertDatabaseMissing('products', $productCreated);
    }

    /** @test */
    public function authenticated_with_permission_can_not_delete_if_product_not_exists()
    {
        $this->loginUserWithPermission('delete_product');
        $productId = -1;
        $response = $this->delete($this->deleteProductRoute($productId));
        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }

    /** @test */
    public function authenticated_without_permission_can_not_delete_product()
    {
        $this->loginUser();
        $productCreated = $this->createFactoryProduct();
        $response = $this->delete($this->deleteProductRoute($productCreated['id']));
        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    public function deleteProductRoute($id): string
    {
        return route('products.delete', $id);
    }

    public function createFactoryProduct(): array
    {
        return Product::factory()->create()->toArray();
    }
}
