<footer class="bg-gray-800">
    <div class="grid grid-cols-2 gap-8 py-8 px-6 md:grid-cols-3" style="width: 1170px;margin: 0px auto">
        <div>
            <h2 class="mb-6 text-sm font-semibold text-gray-400 uppercase">Giới thiệu</h2>
            <ul class="text-gray-300">
                <li class="mb-4">
                    <a href="#" class=" hover:underline">Địa chỉ: Bắc Ninh</a>
                </li>
                <li class="mb-4">
                    <a href="#" class="hover:underline">Email:ngvviet0306@gmail.com</a>
                </li>
                <li class="mb-4">
                    <a href="#" class="hover:underline">Hotline: 0345248120</a>
                </li>
            </ul>
        </div>
        <div>
            <h2 class="mb-6 text-sm font-semibold text-gray-400 uppercase">Liên kết website</h2>
            <ul class="text-gray-300">
                <li class="mb-4">
                    <a href="https://giaygiare.vn/" class="hover:text-white"
                    >Giaygiare.vn</a>
                </li>
            </ul>
        </div>
        <div>
            <h2 class="mb-6 text-sm font-semibold text-gray-400 uppercase">Gọi mua hàng</h2>
            <ul class="text-gray-300">
                <li class="mb-4">
                    <a>0345248120</a>
                </li>
                <li class="mb-4">
                    <a>0353820211</a>
                </li>
            </ul>
        </div>
    </div>
    <div class="py-6 px-4 bg-gray-700">
        <div class="md:flex md:items-center md:justify-center" style="width: 1170px;margin: 0px auto">
                    <span class="text-sm text-gray-300 sm:text-center">© 2022 <a
                            href="https://flowbite.com">Tulshop</a>. All Rights Reserved.
                    </span>
        </div>
    </div>
</footer>
