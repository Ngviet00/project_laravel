<?php

namespace Tests\Feature\Roles;

use App\Models\Role;
use Illuminate\Http\Response;
use Tests\TestCase;

class CreateRoleTest extends TestCase
{

    /** @test */
    public function unauthenticated_can_not_view_form_role_create()
    {
        $response = $this->get($this->getCreateRoleRoute());
        $response->assertRedirect(route('login'));
    }

    /** @test */
    public function authenticated_without_permission_can_not_view_form_role_create()
    {
        $this->loginUser();
        $response = $this->get($this->getCreateRoleRoute());
        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    /** @test */
    public function authenticated_with_permission_can_view_form_role_create()
    {
        $this->loginUserWithPermission('view_role');
        $response = $this->get($this->getCreateRoleRoute());
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('admin.role.create');
    }

    /** @test */
    public function authenticated_without_permission_can_not_create_new_role()
    {
        $this->loginUser();
        $dataCreate = $this->makeFactoryRole();
        $response = $this->post($this->storeRoleRoute(), $dataCreate);
        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    /** @test */
    public function authenticated_with_permission_can_not_create_new_role_if_name_is_null()
    {
        $this->loginUserWithPermission('store_role');
        $dataCreate = $this->makeFactoryRoleSetFieldNull('name');
        $response = $this->post($this->storeRoleRoute(), $dataCreate);
        $response->assertSessionHasErrors(['name']);
    }

    /** @test */
    public function authenticated_with_permission_can_see_name_required_if_data_is_invalid()
    {
        $this->loginUserWithPermission('store_role');
        $dataCreate = $this->makeFactoryRoleSetFieldNull('name');
        $response = $this->from($this->getCreateRoleRoute())->post($this->storeRoleRoute(), $dataCreate);
        $response->assertRedirect($this->getCreateRoleRoute());
    }

    /** @test */
    public function authenticated_with_permission_can_create_new_role_if_data_is_valid()
    {
        $this->loginUserWithPermission('store_role');
        $dataCreate = $this->makeFactoryRole();
        $response = $this->post($this->storeRoleRoute(), $dataCreate);
        $response->assertStatus(Response::HTTP_FOUND);
        $this->assertDatabaseHas('roles', $dataCreate);
        $response->assertRedirect(route('roles.index'));
    }

    public function getCreateRoleRoute(): string
    {
        return route('roles.create');
    }

    public function storeRoleRoute(): string
    {
        return route('roles.store');
    }

    public function makeFactoryRole(): array
    {
        return Role::factory()->make()->toArray();
    }

    public function makeFactoryRoleSetFieldNull($field, $value = null): array
    {
        return Role::factory()->make([
            $field => $value
        ])->toArray();
    }
}
