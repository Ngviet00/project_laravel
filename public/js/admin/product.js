const Product = (function () {
    let modules = {};

    modules.getListBy = function (url, data) {
        Base.callData(url, data)
            .done(function (res) {
                $('#list').html('').append(res);
            })
    }

    modules.getList = function () {
        let url = $('#list').data('action');
        let data = $('.form-search-product').serialize();
        Product.getListBy(url, data);
    }

    modules.create = function () {
        let url = $('#form-create-product').data('action');
        let data = new FormData(document.getElementById('form-create-product'));
        let image = $('#image').get(0).files[0];
        data.append('image', image);
        Base.callWithFormData(url, data, 'POST')
            .done(function (res) {
                $('#modal-create-product').modal('hide');
                CustomAlert.alertSuccess(res.message);
                Product.resetFormCreate();
                Product.getList();
            }).fail(function (res) {
            Base.showError(res.responseJSON.errors);
        });
    }

    modules.edit = function (element) {
        let url = element.data('action');
        Base.callData(url, {}, 'GET')
            .done(function (res) {
                $('.edit-form-product').html('').append(res);
            }).fail(function () {
            CustomAlert.alertError();
        });
    }

    modules.update = function () {
        let url = $('#form-update-product').data('action');
        let data = new FormData(document.getElementById('form-update-product'));
        let image = $('#image').get(0).files[0];
        data.append('image', image ? image : '');
        Base.callWithFormData(url, data, 'post')
            .done(function (res) {
                $('#modal-edit-product').modal('hide');
                $('#form-update-product')[0].reset();
                $('#showImageUpdate').attr("src", "");
                CustomAlert.alertSuccess(res.message);
                Product.getList();
            }).fail(function (res) {
            Base.showError(res.responseJSON.errors);
        });
    }

    modules.delete = function (url) {
        Base.callData(url, {}, 'DELETE')
            .done(function (res) {
                CustomAlert.alertSuccess(res.message);
                Product.getList();
            }).fail(function (err) {
            CustomAlert.alertError();
        });
    }

    modules.resetFormCreate = function () {
        $("#form-create-product")[0].reset();
        $('#showImage').attr("src", "");
        Base.resetError();
    }

    modules.deleteSearch = function () {
        $(".form-search-product")[0].reset();
        Product.getList();
    }

    modules.getListByPageLink = function (element) {
        let url = element.attr('href');
        if (url !== undefined) {
            Product.getListBy(url);
        }
    }

    modules.getListBySearch = function () {
        let url = $('#list').data('action');
        let data = $('.form-search-product').serialize();
        Product.getListBy(url, data);
    }

    modules.deleteProduct = function (element) {
        let url = element.data('action');
        CustomAlert.confirmDelete().then(
            function () {
                Product.delete(url);
            }
        );
    }

    modules.showImage = function (e) {
        let showImage = $('#showImage');
        let file = e.get(0).files[0];
        let reader = new FileReader();
        reader.onload = function (e) {
            showImage.attr('src', e.target.result);
        }
        reader.readAsDataURL(file);
    }

    return modules;
}(window.jQuery, window, document));

$(document).ready(function () {
    Product.getList();

    $('.btn-search-product').on('click', function (e) {
        e.preventDefault();
        Product.getListBySearch();
    })

    $(document).on('click', '.btn-delete-product', function (e) {
        e.preventDefault();
        Product.deleteProduct($(this));
    });

    $(document).on('click', '.btn-show-modal-edit-product', function () {
        Product.edit($(this));
    });

    $(document).on('click', '.page-link', function (e) {
        e.preventDefault();
        Product.getListByPageLink($(this));
    });

    $(document).on('change', '#image', function (e) {
        Product.showImage($(this));
    });

    $('.btn-store-product').on('click', function (e) {
        e.preventDefault();
        Product.create();
    });

    $('.btn-show-modal-create-product').on('click', function () {
        Product.resetFormCreate();
    })

    $('.btn-update-product').on('click', function (e) {
        e.preventDefault();
        Product.update();
    })

    $('.btn-delete-search').on('click', function () {
        Product.deleteSearch();
    });
});
