<?php

use App\Http\Controllers\BrandController;
use Illuminate\Support\Facades\Route;

Route::name('brands.')->prefix('brands')->group(function () {
    Route::get('/', [BrandController::class, 'index'])
        ->name('index');

    Route::get('/create', [BrandController::class, 'create'])
        ->name('create');

    Route::post('/', [BrandController::class, 'store'])
        ->name('store');

    Route::get('/{id}', [BrandController::class, 'show'])
        ->name('show');

    Route::get('/edit/{id}', [BrandController::class, 'edit'])
        ->name('edit');

    Route::put('/edit/{id}', [BrandController::class, 'update'])
        ->name('update');

    Route::delete('/{id}', [BrandController::class, 'destroy'])
        ->name('delete');
});
