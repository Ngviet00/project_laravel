@extends('admin.layouts.app')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card my-4">
                    <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
                        <div class="bg-gradient-primary shadow-primary border-radius-lg pt-4 pb-3">
                            <h6 class="text-white text-capitalize ps-3">Update User</h6>
                        </div>
                    </div>

                    <div class="px-0 pb-2">
                        <a href="{{route('users.index')}}" style="margin-left: 17px;"
                           class="mb-0 mt-2 btn btn-dark d-inline-block">Back</a>
                        <form method="POST" action="{{route('users.update', $user)}}" class="p-4">
                            @csrf
                            @method('put')
                            <div class="col-xs-12 col-sm-12 col-md-12 mt-3">
                                <div class="form-group custom-form-group-create">
                                    <strong class="form-check">Name: </strong>
                                    <div class="form-check input-group input-group-dynamic info-horizontal">
                                        <input type="text" name="name" id="name" class="form-control shadow-none"
                                               style="outline: none" value="{{ $user->name }}" onfocus="focused(this)"
                                               onfocusout="defocused(this)">
                                    </div>
                                    @error('name')
                                    <div class="text-danger my-1">{{ $message }} </div> @enderror
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 mt-3">
                                <div class="form-group custom-form-group-create">
                                    <strong class="form-check">Email: </strong>
                                    <div class="form-check input-group input-group-dynamic info-horizontal">
                                        <input type="email" name="email" id="email" class="form-control shadow-none"
                                               style="outline: none" value="{{ $user->email }}" onfocus="focused(this)"
                                               onfocusout="defocused(this)">
                                    </div>
                                    @error('email')
                                    <div class="text-danger my-1">{{ $message }} </div> @enderror
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 mt-3">
                                <div class="form-group custom-form-group-create">
                                    <strong class="form-check">Address: </strong>
                                    <div class="form-check input-group input-group-dynamic info-horizontal">
                                        <input type="text" name="address" id="address" class="form-control shadow-none"
                                               style="outline: none" value="{{ $user->address }}"
                                               onfocus="focused(this)" onfocusout="defocused(this)">
                                    </div>
                                    @error('address')
                                    <div class="text-danger my-1">{{ $message }} </div> @enderror
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 mt-3">
                                <div class="form-group custom-form-group-create">
                                    <strong class="form-check">Date of Bidth: </strong>
                                    <div class="form-check input-group input-group-dynamic info-horizontal">
                                        <input type="date" name="dob" id="dob" class="form-control shadow-none"
                                               style="outline: none" value="{{$user->dob}}" onfocus="focused(this)"
                                               onfocusout="defocused(this)">
                                    </div>
                                    @error('dob')
                                    <div class="text-danger my-1">{{ $message }} </div> @enderror
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 mt-3">
                                <div class="form-group custom-form-group-create">
                                    <strong class="form-check">Phone: </strong>
                                    <div class="form-check input-group input-group-dynamic info-horizontal">
                                        <input type="tel" name="phone" id="phone" class="form-control shadow-none"
                                               style="outline: none" value="{{ $user->phone }}" onfocus="focused(this)"
                                               onfocusout="defocused(this)">
                                    </div>
                                    @error('phone')
                                    <div class="text-danger my-1">{{ $message }} </div> @enderror
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 mt-3">
                                <div class="form-group custom-form-group-create">
                                    <strong class="form-check mb-2">Gender: </strong>
                                    <div class="d-flex">
                                        <div class="d-flex align-items-center" style="margin-right: 20px;">
                                            <input type="radio" name="gender" id="nam"
                                                   value="1" {{ $user->gender == 1 ? 'checked' : '' }}>
                                            <label for="nam" class="text-dark"
                                                   style="margin: 0;margin-left: 5px">Nam</label>
                                        </div>
                                        <div class="d-flex align-items-center">
                                            <input type="radio" name="gender" id="nu" value="0"
                                                   {{ $user->gender == 0 ? 'checked' : '' }}}>
                                            <label for="nu" class="text-dark"
                                                   style="margin: 0; margin-left:5px">Nu</label>
                                        </div>
                                    </div>
                                    @error('gender')
                                    <div class="text-danger my-1">{{ $message }} </div> @enderror
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 mt-3">
                                <div class="form-group custom-form-group-create">
                                    <strong class="form-check">Password </strong>
                                    <div class="form-check input-group input-group-dynamic info-horizontal">
                                        <input type="password" name="password" id="password"
                                               class="form-control shadow-none" style="outline: none" value=""
                                               onfocus="focused(this)" onfocusout="defocused(this)">
                                    </div>
                                    @error('password')
                                    <div class="text-danger my-1">{{ $message }} </div> @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <strong class="form-check" style="padding-left: 0px;">Role </strong>
                                @foreach($roles as $role)
                                    <div class="form-check d-flex align-items-center">
                                        <input type="checkbox" value="{{ $role->id }}" name="role[]" id="{{$role->id}}"
                                               class="form-input @error('role') is-invalid @enderror"
                                            {{ $user->roles->contains($role->id) ? 'checked' : '' }}
                                        >
                                        <label for="{{$role->id}}" class="m-0 text-dark"
                                               style="display:block;margin-left:5px !important;font-size:18px">{{ $role->name }}</label>
                                    </div>
                                @endforeach
                                @error('role')
                                <div class="text-danger">{{ $message }} </div> @enderror
                            </div>
                            <button type="submit" class=" mt-4 btn btn-danger">Update</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
