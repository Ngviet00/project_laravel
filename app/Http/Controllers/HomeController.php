<?php

namespace App\Http\Controllers;

use App\Models\Blog;
use App\Models\Brand;
use App\Models\Category;
use App\Models\OrderDetail;
use App\Models\Product;
use App\Models\Size;
use App\Models\Slide;
use App\Models\UserOrder;
use App\Services\ProductService;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    protected ProductService $productService;

    public function __construct(ProductService $productService)
    {
    }

    public function dashboard()
    {
        $order = OrderDetail::all();

        $orderWait = $order->filter(function ($item) {
            return $item->status == 1;
        })->count();

        $orderIsTransport = $order->filter(function ($item) {
            return $item->status == 2;
        })->count();

        $orderSuccess = $order->filter(function ($item) {
            return $item->status == 3;
        })->count();

        $orderCancel = $order->filter(function ($item) {
            return $item->status == 4;
        })->count();

        $totalOrder = $order->filter(function ($item) {
            return $item->status == 3;
        })->sum('total');
        return view('admin.dashboard',
            compact(
                'orderWait',
                'orderIsTransport',
                'orderSuccess',
                'orderCancel',
                'totalOrder'));
    }

    public function index()
    {
        $slides = Slide::orderBy('id', 'desc')->paginate(3);
        $blogs = Blog::orderBy('id', 'desc')->paginate(3);
        $brands = Brand::orderBy('id', 'desc')->paginate(3);
        $products = Product::orderBy('id', 'desc')->get();

        $trendings = $products->filter(function ($item) {
            return $item->trending == 0;
        })->take(4)->values();

        $latests = $products->filter(function ($item) {
            return $item->trending == 1;
        })->take(8)->values();

        return view('welcome', compact('slides', 'blogs', 'brands', 'trendings', 'latests'));
    }

    public function show(Product $product)
    {
        $product = $product->load(['brands', 'sizes']);
        $relateProduct = Product::where([
            ['category_id', '=', $product->category_id],
            ['id', '!=', $product->id]
        ])->get();
        return view('client.show', compact('product', 'relateProduct'));
    }

    public function checkout()
    {
        $dataSessions = session('userOrder') ? session('userOrder') : null;;
        return view('client.checkout', compact('dataSessions'));
    }

    public function cart()
    {
        return view('client.cart');
    }

    public function loginOrder()
    {
        return view('client.login');
    }


    public function blog($id)
    {
        $blog = Blog::findOrFail($id);
        return view('client.blog', compact('blog'));
    }

    public function filterProduct(Request $request)
    {
        $sizes = Size::all();
        $brands = Brand::all();
        $categories = Category::all();
        if (empty($request->all())) {
            $products = Product::all();
            return view('client.filter_product', compact('sizes', 'brands', 'categories', 'products'));
        } else {
            $result = Product::query();
            if ($request->sort) {
                $result = $result->orderBy('price', $request->sort);
            }
            if ($request->brands) {
                $result = $result->where('brand_id', $request->brands);
            }
            if ($request->category) {
                $result = $result->where('category_id', $request->category);
            }
            if ($request->price) {
                switch ($request->price) {
                    case 1:
                        $result = $result->where('price', '<', '1000000');
                        break;
                    case 2:
                        $result = $result->whereBetween('price', [1000000, 2000000]);
                        break;
                    case 3:
                        $result = $result->whereBetween('price', [2000000, 5000000]);
                        break;
                    case 4:
                        $result = $result->where('price', '>', '5000000');
                        break;
                    default:
                        null;
                }
            }
            $products = $result->get();
            $listRequests = $request->all();
            return view('client.filter_product', compact('sizes', 'brands', 'categories', 'products', 'listRequests'));
        }
    }

    public function order(Request $request)
    {
        $userOrder = UserOrder::where('phone', $request->phone)->first();
        if ($userOrder) {
            foreach (session('cart') as $cartItem) {
                OrderDetail::create([
                    'name' => $cartItem['name'],
                    'price' => $cartItem['price'],
                    'quantity' => $cartItem['quantity'],
                    'date_order' => now(),
                    'image' => $cartItem['image'],
                    'size' => $cartItem['size'],
                    'status' => 1,
                    'phone' => $userOrder->phone,
                    'total' => $cartItem['total'],
                    'user_order_id' => $userOrder->id
                ]);
            }
        } else {
            $newUserOrder = UserOrder::create($request->all());
            foreach (session('cart') as $cartItem) {
                OrderDetail::create([
                    'name' => $cartItem['name'],
                    'price' => $cartItem['price'],
                    'quantity' => $cartItem['quantity'],
                    'date_order' => now(),
                    'image' => $cartItem['image'],
                    'size' => $cartItem['size'],
                    'status' => 1,
                    'phone' => $newUserOrder->phone,
                    'total' => $cartItem['total'],
                    'user_order_id' => $newUserOrder->id
                ]);
            }
        }
        session()->put('cart', []);
        return response()->json('Đặt hàng thành công!');
    }

    public function historyOrder(Request $request)
    {
        $userOrder = UserOrder::where('phone', $request->phone)->first();
        session()->put('userOrder', $userOrder->toArray());
        session()->put('phone', $request->phone);
        $listOrderHistory = OrderDetail::where('phone', $request->phone)->get();
        return view('client.history_order', compact('userOrder', 'listOrderHistory'));
    }

    public function listOrder(Request $request)
    {
        if (!empty($request->all())) {
            $listOrderHistory = OrderDetail::orderBy('id', 'desc')
                ->where('id', $request->id_order)
                ->orWhere('status', $request->status)->get();
        } else {
            $listOrderHistory = OrderDetail::orderBy('id', 'desc')->get();
        }
        return view('admin.order.index', compact('listOrderHistory'));
    }

    public function logoutPhone(): \Illuminate\Http\RedirectResponse
    {
        session()->forget('phone');
        session()->forget('userOrder');
        return redirect()->route('history-order');
    }

    public function updateOrder($id, $value)
    {
        $orderDetail = OrderDetail::findOrFail($id);
        $orderDetail->update([
            'status' => $value
        ]);
        return redirect()->route('listOrder');
    }

    public function detailOrder($id)
    {
        $detailOrder = OrderDetail::with('userOrder')->findOrFail($id);
        return view('admin.order.detail', compact('detailOrder'));
    }
}
